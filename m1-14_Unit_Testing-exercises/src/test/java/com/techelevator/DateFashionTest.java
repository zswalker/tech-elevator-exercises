package com.techelevator;

import org.junit.*;

public class DateFashionTest {
	private DateFashion test;
	
	@Before
	public void setup() {
		test = new DateFashion();
	}
	@Test
	public void you_and_date_less_than_2_return_0() {
		int tableInt = test.getATable(0, 0);
		
		Assert.assertEquals(0, tableInt);
	}
	@Test
	public void you_less_than_2_date_greater_than_8_return_0() {
		int tableInt = test.getATable(1, 9);
		
		Assert.assertEquals(0, tableInt);
	}
	@Test
	public void you_and_date_greater_than_8_return_2() {
		int tableInt = test.getATable(9, 10);
		
		Assert.assertEquals(2, tableInt);
	}
	@Test
	public void you_and_date_greater_than_2_less_than_8_return_1() {
		int tableInt = test.getATable(5, 6);
		
		Assert.assertEquals(1, tableInt);
	}
	@Test
	public void you_greater_than_2_less_than_8_date_greater_than_8_return_2() {
		int tableInt = test.getATable(4, 9);
		
		Assert.assertEquals(2, tableInt);
	}
	@Test
	public void pass_in_negative_rating_return_0() {
		int tableInt = test.getATable(-2, -1);
		
		Assert.assertEquals(0, tableInt);
	}
	@Test
	public void pass_in_greater_than_10_rating_for_both_return_2() {
		int tableInt = test.getATable(11, 12);
		
		Assert.assertEquals(2, tableInt);
	}
	@Test
	public void date_less_than_2_you_greater_than_2_less_than_8_return_0() {
		int tableInt = test.getATable(6, 0);
		
		Assert.assertEquals(0, tableInt);
	}
}
