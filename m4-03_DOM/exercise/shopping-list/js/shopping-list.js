// add pageTitle
let pageTitle = 'My Shopping List';
// add groceries
let groceries = ['milk', 'cereal', 'coffee', 'bread', 'peanut butter', 'jelly', 'peas', 'broccoli', 'chicken', 'salmon']

/**
 * This function will get a reference to the title and set its text to the value
 * of the pageTitle variable that was set above.
 */
function setPageTitle() {
  let titleContainer = document.getElementById('title');
  titleContainer.innerText = pageTitle;
}

/**
 * This function will loop over the array of groceries that was set above and add them to the DOM.
 */
function displayGroceries() {
  let groceryListContainer = document.querySelector('.shopping-list ul');
  groceries.forEach( (item) => {
    let groceryListItem = document.createElement('li');
    groceryListItem.innerText = item;
    groceryListContainer.appendChild(groceryListItem);
  })
}

/**
 * This function will be called wh4en the button is clicked. You will need to get a reference
 * to every list item and add the class completed to each one
 */
function markCompleted() {
  let groceriesListItems = document.querySelectorAll('li');
  groceriesListItems.forEach((item) => {
    item.setAttribute('class', "completed");
  })
}

setPageTitle();

displayGroceries();

// Don't worry too much about what is going on here, we will cover this when we discuss events.
document.addEventListener('DOMContentLoaded', () => {
  // When the DOM Content has loaded attach a click listener to the button
  const button = document.querySelector('.btn');
  button.addEventListener('click', markCompleted);
});
