<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
	<head>
		<title>Exercise 2 - Fibonacci 25</title>
		<style>
			li {
				list-style-type: none;
			}
		</style>
	</head>
	<body>
	<c:set var="currentNum" value="0"/>
	<c:set var="previousNum" value="0"/>
	<c:set var="previousPreviousNum" value="0"/>
	<c:set var="initialIncrement" value="1"/>
		<h1>Exercise 2 - Fibonacci 25</h1>
		<ul>
			<c:forEach begin="1" end="25" var="num">
					<c:if test="${currentNum == 1 && initialIncrement == 1}">
						<c:set var="initialIncrement" value="0"/>
					</c:if>
					<li>${currentNum}</li>
						<c:set var="currentNum" value="${previousNum+previousPreviousNum+initialIncrement}"/>
						<c:set var="previousPreviousNum" value="${previousNum}"/>
						<c:set var="previousNum" value="${currentNum}"/>
			</c:forEach>
			<%--
				Add a list item (i.e. <li>) for each of the first 25 numbers in the Fibonacci sequence
				
				See exercise2-fibonacci.png for example output
			 --%>
		</ul>
	</body>
</html>