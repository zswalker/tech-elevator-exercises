package com.techelevator;

import java.text.DecimalFormat;
import java.util.Scanner;

public class LinearConvert {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		DecimalFormat df = new DecimalFormat("#.00");
		
	System.out.print("Please enter the length: ");
	double userLength = in.nextDouble();
	in.nextLine();
	
	System.out.print("Is your value in (M)eters or (F)eet?: ");
	String userMeasureType = in.next();
	in.nextLine();
	
	String oppMeasureType = "";
	double convertedLength = 0.0;
	double metersToFeetConversion = 3.2808399;
	double feetToMetersConversion = 0.3048;
	int checkMeasureType = 0;
	
		if (userMeasureType.equals("m") || userMeasureType.equals("M")) {
			oppMeasureType = "F";
			//directly below is to capitalize userMeasureType
			userMeasureType ="M";
			convertedLength = userLength * metersToFeetConversion;
		} else if (userMeasureType.equals("f") || userMeasureType.equals("F")) {
			oppMeasureType = "M";
			//directly below is to capitalize userMeasureType
			userMeasureType = "F";
			convertedLength = userLength * feetToMetersConversion;
		} else {
			System.out.print("Invalid Measure Type");
			checkMeasureType++;
		}
	
	String formattedUserLength = df.format(userLength);
	String formattedConvertedLength = df.format(convertedLength);
	
		if(checkMeasureType == 0) {
			System.out.println(formattedUserLength + userMeasureType + " is " + formattedConvertedLength + oppMeasureType);
		}
	
	}
}
