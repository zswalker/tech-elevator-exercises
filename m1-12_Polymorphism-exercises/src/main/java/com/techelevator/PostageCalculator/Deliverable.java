package com.techelevator.PostageCalculator;

public interface Deliverable {
	double calculateRate(int distance, double weight);
	String getName();
}
