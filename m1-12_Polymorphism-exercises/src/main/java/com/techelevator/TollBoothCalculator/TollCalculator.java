package com.techelevator.TollBoothCalculator;

import java.util.ArrayList;
import java.util.List;

public class TollCalculator {

	public static void main(String[] args) {
		List<Tollable> vehiclesTolledList = new ArrayList<Tollable>();
			vehiclesTolledList.add(new Car(false));
			vehiclesTolledList.add(new Car(true));
			vehiclesTolledList.add(new Tank());
			vehiclesTolledList.add(new Truck(4));
			vehiclesTolledList.add(new Truck(6));
			vehiclesTolledList.add(new Truck(8));
			
			System.out.printf("%-23s %-23s Toll $\n", "Vehicle", "Distance Traveled");
			System.out.println("------------------------------------------------------");
			
			int totalMilesTraveled = 0;
			double totalTollRevenue = 0;
			
			for (Tollable vehicle : vehiclesTolledList) {
				int randomDistance = (int)((Math.random() * (240 - 10)) + 10);
				System.out.printf("%-23s %-23s $%-4.2f\n", vehicle.toString(), randomDistance, vehicle.calculateToll(randomDistance));
				totalMilesTraveled += randomDistance;
				totalTollRevenue += vehicle.calculateToll(randomDistance);
			}
			System.out.println("\nTotal Miles Traveled: " + totalMilesTraveled);
			System.out.printf("Total Tollbooth Revenue: $%-4.2f", totalTollRevenue);
	}

}

//if (vehicle instanceof Car) {
//	Car tollableAsCar = (Car)vehicle;
//	if (tollableAsCar.isTrailerEquipped()) {
//		System.out.printf("%-23s %-23s $%-4.2f\n", "Car (with trailer)", randomDistance, vehicle.calculateToll(randomDistance));
//	} else {
//		System.out.printf("%-23s %-23s $%-4.2f\n", "Car", randomDistance, vehicle.calculateToll(randomDistance));
//	}
//} else if (vehicle instanceof Truck) {
//	Truck tollableAsTruck = (Truck) vehicle;
//	if (tollableAsTruck.getNumberOfAxles() == 4) {
//		System.out.printf("%-23s %-23s $%-4.2f\n", "Truck (with 4 axels)", randomDistance, vehicle.calculateToll(randomDistance));
//	} else if (tollableAsTruck.getNumberOfAxles() == 6) {
//		System.out.printf("%-23s %-23s $%-4.2f\n", "Truck (with 6 axels)", randomDistance, vehicle.calculateToll(randomDistance));
//	} else {
//		System.out.printf("%-23s %-23s $%-4.2f\n", "Truck (8 axels)", randomDistance, vehicle.calculateToll(randomDistance));
//	}
//} else {
//	System.out.printf("%-23s %-23s $%-4.2f\n", "Tank", randomDistance, vehicle.calculateToll(randomDistance));
//}
