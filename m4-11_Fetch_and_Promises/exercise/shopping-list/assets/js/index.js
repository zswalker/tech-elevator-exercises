
let shoppingItems= [];

document.addEventListener('DOMContentLoaded', () => {

    document.querySelector('.loadingButton').addEventListener('click', () => {

        loadList();
        
        })

})



function loadList() {
    fetch('assets/data/shopping-list.json')
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        shoppingItems = data;
        displayList();
    })
    .catch( (err) => {
        console.log(err);
    })

}

function displayList() {

    if ('content' in document.createElement('template')) {

       const shopListContainer =  document.querySelector('ul');

        shoppingItems.forEach((item) => {
            const tmplt = document.querySelector('template').content.cloneNode(true);
            const txtNode = document.createTextNode(item.name);
            tmplt.querySelector('li').appendChild(txtNode);

            if (item.completed) {
                const li = tmplt.querySelector('li')
                li.classList.add('completed');
                li.querySelector('.fa-check-circle').classList.add('completed');
            }


            shopListContainer.appendChild(tmplt);
        })

        
    
        
    }

}
