package com.techelevator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class FileSplitter {

	public static void main(String[] args) throws IOException {
		File userFile = getFileFromUser();
		int maxLinesPerFile = getLineMaximumFromUser();
		int totalLines = getTotalLineCountInFile(userFile);
		double copiesNeeded = getTotalNumberOfCopiesNeeded(totalLines, maxLinesPerFile);
		int copiesNeededAsInt = (int)copiesNeeded;
		
		System.out.println("\n" + userFile.getName() + " has " + totalLines + " lines of text.\n");
		System.out.println("For a " + totalLines + " line input file this will create " + copiesNeededAsInt + " output files.\n");
		System.out.println("**GENERATING OUTPUT**\n");
		
		generateFiles(userFile, copiesNeeded, maxLinesPerFile);
		
		System.out.println("\n**GENERATION COMPLETE**");
		
	}
	private static File getFileFromUser() {
		Scanner userInput = new Scanner(System.in); {
			System.out.println("Where is the input file? (please include the file path)");
			String path = userInput.nextLine();
		
			File inputFile = new File(path);
			if (!inputFile.exists()) {
				System.out.println(path + " does not exist");
				System.exit(1);
			} else if (!inputFile.isFile()) {
				System.out.println(path + " is not a file");
				System.exit(1);
			}
			return inputFile;
		}
	}
	private static int getLineMaximumFromUser() {
		Scanner userInput = new Scanner(System.in);
		System.out.println("How many lines of text (max) should there be in the split files?");
		int max = userInput.nextInt();
		return max;
	}
	private static int getTotalLineCountInFile(File file) throws FileNotFoundException {
		try (Scanner fileReader = new Scanner(file);) {
			int lineCount = 0;
			while (fileReader.hasNextLine()) {
				fileReader.nextLine();
				lineCount++;
			}
			return lineCount;
		}
	}
	private static String getNewFileName(File userFile, int fileNumber) {
		return userFile.getName().substring(0, userFile.getName().indexOf(".")) + "-" + fileNumber + userFile.getName().substring(userFile.getName().indexOf("."));
	}
	private static double getTotalNumberOfCopiesNeeded(int totalLineCount, int maxLines) {
		return (Math.ceil((double)totalLineCount / maxLines));
	}
	private static void generateFiles(File userFile, double copiesNeeded, int maxLines) throws IOException {
		Scanner fileReader = new Scanner(userFile);
		
		for (int i = 1; i <= copiesNeeded; i++) {
			File newFile = new File(getNewFileName(userFile, i));
			newFile.createNewFile();
			System.out.println("Generating " + newFile.getName());
			PrintWriter fileWriter = new PrintWriter(newFile);
			writeFiles(fileWriter, fileReader, maxLines);
		}
	}
	private static void writeFiles(PrintWriter fileWriter, Scanner fileReader, int maxLines) {
		int lineCount = 1;
		
		while (fileReader.hasNextLine()) {
			if (lineCount <= maxLines) {
				String line = fileReader.nextLine();
				fileWriter.println(line);
				lineCount++;
				fileWriter.flush();
			} else {
				lineCount = 1;
				break;
			}				
		}
	}
}
