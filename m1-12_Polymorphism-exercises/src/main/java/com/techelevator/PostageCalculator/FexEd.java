package com.techelevator.PostageCalculator;

public class FexEd implements Deliverable {
	private String name = "FexEd";
	
	@Override
	public double calculateRate(int distanceInMiles, double weightInOunces) {
		if (distanceInMiles <= 500 && weightInOunces <= 48) {
			return 20.00;
		} else if (distanceInMiles > 500 && weightInOunces <= 48) {
			return 25.00;
		} else if (distanceInMiles <= 500 && weightInOunces > 48) {
			return 23.00;
		} else {
			return 28.00;
		}
		
	}
	public String getName() {
		return name;
	}
	
}
