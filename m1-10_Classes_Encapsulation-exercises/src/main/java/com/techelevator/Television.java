package com.techelevator;

public class Television {

	private boolean power = false;
	private int currentChannel = 3;
	private int currentVolume = 2;
	
	public void turnOff() {
		power = false;
	}
	
	public void turnOn() {
		power = true;
		currentChannel = 3;
		currentVolume = 2;
	}
	
	public void changeChannel(int newChannel) {
		if (power && newChannel >= 3 && newChannel <= 18) {
			currentChannel = newChannel;
		}
	}
	
	public void channelUp() {
		if (power && currentChannel >= 3 && currentChannel < 18) {
			currentChannel++;
		} else if (power && currentChannel == 18) {
			currentChannel = 3;
		}
	}
	
	public void channelDown() {
		if (power && currentChannel > 3 && currentChannel <= 18) {
			currentChannel--;
		} else if (power && currentChannel == 3) {
			currentChannel = 18;
		}
	}
	
	public void raiseVolume() {
		if (power && currentVolume < 10) {
			currentVolume++;
		}
	}
	
	public void lowerVolume() {
		if (power && currentVolume > 0) {
			currentVolume--;
		}
	}
	
	public boolean isOn() {
		return power;
	}
	
	public int getCurrentChannel() {
		return currentChannel;
	}
	
	public int getCurrentVolume() {
		return currentVolume;
	}
	
}
