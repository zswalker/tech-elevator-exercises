package com.techelevator;

import org.junit.*;

public class MaxEnd3Test {
	private MaxEnd3 test;
	
	@Before
	public void setup() {
		test = new MaxEnd3();
	}
	@Test
	public void last_element_larger_than_first_return_array_of_all_elements_equal_to_last() {
		int[] array = {1, 2, 7};
		int[] expected = {7, 7, 7};
		int[] result = test.makeArray(array);
		Assert.assertArrayEquals(expected, result);
	}
	@Test
	public void first_element_larger_than_first_return_array_of_all_elements_equal_to_last() {
		int[] array = {7, 2, 6};
		int[] expected = {7, 7, 7};
		int[] result = test.makeArray(array);
		Assert.assertArrayEquals(expected, result);
	}
	@Test
	public void first_and_last_element_equal_return_array_of_all_elements_equal_to_both() {
		int[] array = {7, 2, 7};
		int[] expected = {7, 7, 7};
		int[] result = test.makeArray(array);
		Assert.assertArrayEquals(expected, result);
	}
	@Test
	public void all_elements_equal_returns_unchanged_array() {
		int[] array = {7, 7, 7};
		int[] expected = {7, 7, 7};
		int[] result = test.makeArray(array);
		Assert.assertArrayEquals(expected, result);
	}
}
