package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WordSearch {

	public static void main(String[] args) throws FileNotFoundException {
		File inputFile = getFileFromUser();
		String word = getWordFromUser();
		boolean isCaseSensitive = false;
		
		try {
			isCaseSensitive = getIsCaseSensitiveFromUser(); 
		} catch (NotYOrNException e) {
			System.out.println("You did not enter Y or N");
		}
		
		searchForWord(inputFile, word, isCaseSensitive);

	}
	private static File getFileFromUser() {
		Scanner userInput = new Scanner(System.in);
		System.out.println("What is the file that should be searched?");
		String path = userInput.nextLine();
		
		File inputFile = new File(path);
		if (!inputFile.exists()) {
			System.out.println(path + " does not exist");
			System.exit(1);
		} else if (!inputFile.isFile()) {
			System.out.println(path + " is not a file");
			System.exit(1);
		}
		return inputFile;
	}
	private static String getWordFromUser() {
		Scanner userInput = new Scanner(System.in);
		System.out.println("What is the search word you are looking for?");
		String word = userInput.next();
		userInput.nextLine();
		
		return word;
	}
	private static void searchForWord(File inputFile, String word, boolean isCaseSensitive) throws FileNotFoundException {
		try (Scanner fileIn = new Scanner(inputFile)) {
			boolean isWordInText = false;
			int lineCount = 1;
			if (isCaseSensitive) {
				while (fileIn.hasNextLine()) {
					String line = fileIn.nextLine();
						if (line.contains(word)) {
							System.out.println(lineCount + ") " + line);
							isWordInText = true;
						}
				lineCount++;
				}
			}
			if (!isCaseSensitive) {
				while (fileIn.hasNextLine()) {
					String line = fileIn.nextLine();
					String lineToLowerCase = line.toLowerCase();
					String wordToLowerCase = word.toLowerCase();
						if (lineToLowerCase.contains(wordToLowerCase)) {
							System.out.println(lineCount + ") " + line);
							isWordInText = true;
						}
					lineCount++;
				}
			}
			if (!isWordInText) {
				System.out.println("Word not found");
			}
		}
	}
	private static boolean getIsCaseSensitiveFromUser() throws NotYOrNException {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Should the search be case sensitive? (Y\\N)");
		String yOrN = userInput.next().toUpperCase();
		userInput.nextLine();
		if (yOrN.equals("Y") || yOrN.equals("N")) {
			return (yOrN.equals("Y"));
		} else {
			throw new NotYOrNException("Submission must be \"Y\" or \"N\"");
		}

	}
}
