package com.techelevator.TollBoothCalculator;

public interface Tollable {
	double calculateToll(int distance);
}
