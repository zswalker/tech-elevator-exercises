package com.techelevator;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.techelevator.model.Review;
import com.techelevator.model.ReviewDao;

@Controller 
public class HelloController {

	@Autowired
	ReviewDao reviewDao;
	
	@RequestMapping("/greeting")
	public String displayGreeting(ModelMap map) {
		
		map.addAttribute("allReviews",reviewDao.getAllReviews());
		
		return "greeting";
	}
	
	@RequestMapping(path="/postReview", method=RequestMethod.POST)
	public String redirectToHome(@RequestParam String username, @RequestParam String reviewTitle, 
			@RequestParam String rating, @RequestParam String reviewText) {
		
		int ratingAsInt = validateRating(rating);
		
		Review review = new Review();
		review.setDateSubmitted(LocalDateTime.now());
		review.setRating(ratingAsInt);
		review.setTitle(reviewTitle);
		review.setUsername(username);
		review.setText(reviewText);
		
		reviewDao.save(review);
		
		return "redirect:/greeting";
	}
	
	private int validateRating(String rating) {
		int ratingAsInt = Integer.parseInt(rating);
		
		if (ratingAsInt < 1) {
			ratingAsInt = 1;
		} else if (ratingAsInt > 5) {
			ratingAsInt = 5;
		}
		
		return ratingAsInt;
	}
	
}
