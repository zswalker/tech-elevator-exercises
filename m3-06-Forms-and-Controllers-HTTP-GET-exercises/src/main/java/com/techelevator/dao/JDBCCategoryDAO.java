package com.techelevator.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.dao.model.Category;

@Component
public class JDBCCategoryDAO implements CategoryDAO{

	 private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JDBCCategoryDAO(DataSource datasource) {
		this.jdbcTemplate = new JdbcTemplate(datasource);
	}
	
	@Override
	public List<Category> getCategoryList() {
		List<Category> categoryList = new ArrayList<>();
		
		String sqlGetGenres = "SELECT category_id, name FROM category";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetGenres);
		
		while (results.next()) {
			Category category = MapRowToCategory(results);
			categoryList.add(category);
		}
		
		
		return categoryList;
		
	}
	
	private Category MapRowToCategory(SqlRowSet results) {
		Category category = new Category();
		category.setCategoryId(results.getLong("category_id"));
		category.setName(results.getString("name"));
		
		return category;
	}

}
