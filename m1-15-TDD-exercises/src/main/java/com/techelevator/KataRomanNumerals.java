package com.techelevator;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class KataRomanNumerals {
	private Map<Character, Integer> romanMap = new HashMap<Character, Integer>();
	private Map <Integer, String> integerMap = new LinkedHashMap<Integer, String>();
	public KataRomanNumerals() {
		romanMap.put('I', 1);
		romanMap.put('V', 5);
		romanMap.put('X', 10);
		romanMap.put('L', 50);
		romanMap.put('C', 100);
		romanMap.put('D', 500);
		romanMap.put('M', 1000);
		
		integerMap.put(1000, "M");
		integerMap.put(900, "CM");
		integerMap.put(500, "D");
		integerMap.put(400, "CD");
		integerMap.put(100, "C");
		integerMap.put(90, "XC");
		integerMap.put(50, "L");
		integerMap.put(40, "XL");
		integerMap.put(10, "X");
		integerMap.put(9, "IX");
		integerMap.put(5, "V");
		integerMap.put(4, "IV");
		integerMap.put(1, "I");
	}
	public String convertToRomanNumeral(int n) {
		String romanNumeral = "";
		int remainder = n;
		
		if (isInvalidAsRomanNumeral(n)) {
			return "invalid";
		}
		for (int key : integerMap.keySet()) {
			int numberOfTimesKeyFitsIntoSubmittedNumber = 0;
			if (remainder / key >= 1) {
				numberOfTimesKeyFitsIntoSubmittedNumber = remainder / key;
				int x = 0;
				while (x < numberOfTimesKeyFitsIntoSubmittedNumber) {
					romanNumeral += integerMap.get(key);
					x++;
				}
				remainder -= (key * numberOfTimesKeyFitsIntoSubmittedNumber);
			}
		}
				
//		while (remainder != 0) {
//			if (integerMap.containsKey(remainder)) {
//				romanNumeral += integerMap.get(remainder);
//				remainder -= remainder;
//			} else if (remainder >= 1000) {
//				romanNumeral += integerMap.get(1000);
//				remainder -= 1000;
//			} else if (remainder >= 900 && remainder <= 999) {
//				romanNumeral += integerMap.get(900);
//				remainder -= 900;
//			} else if (remainder >= 500 && remainder <= 899) {
//				romanNumeral += integerMap.get(500);
//				remainder -= 500;
//			} else if (remainder >= 400 && remainder <= 499) {
//				romanNumeral += integerMap.get(400);
//				remainder -= 400;
//			} else if (remainder >= 100 && remainder <= 399) {
//				romanNumeral += integerMap.get(100);
//				remainder -= 100;
//			} else if (remainder >= 90 && remainder <= 99) {
//				romanNumeral += integerMap.get(90);
//				remainder -= 90;
//			} else if (remainder >= 50 && remainder <= 89) {
//				romanNumeral += integerMap.get(50);
//				remainder -= 50;
//			} else if (remainder >= 40 && remainder <= 49) {
//				romanNumeral += integerMap.get(40);
//				remainder -= 40;
//			} else if (remainder >= 10 && remainder <= 39) {
//				romanNumeral += integerMap.get(10);
//				remainder -= 10;
//			} else if (remainder == 9) {
//				romanNumeral += integerMap.get(9);
//				remainder -= 9;
//			} else if (remainder >= 5 && remainder <= 8) {
//				romanNumeral += integerMap.get(5);
//				remainder -= 5;
//			} else if (remainder == 4) {
//				romanNumeral += integerMap.get(4);
//				remainder -= 4;
//			} else if (remainder > 0 && remainder <= 3) {
//				romanNumeral += integerMap.get(1);
//				remainder -= 1;
//			}
//		}
 			return romanNumeral;
	}
	private boolean isInvalidAsRomanNumeral(int n) {
			return (n <= 0);
	}
	public int convertToDigit(String romanNumeral) {
		if (isEmptyOrNull(romanNumeral) || isInvalidKey(romanNumeral)) {
			return 0;
		}
		return addRomanNumeralSingleDigitsAndGroupings(romanNumeral);
	}
	private boolean isInvalidKey(String romanNumeral) {
		String romanNumeralToUpperCase = romanNumeral.toUpperCase();
		boolean charCheck = false;
		for (int i = 0; i < romanNumeralToUpperCase.length(); i++) {
			if(!romanMap.containsKey(romanNumeralToUpperCase.charAt(i))) {
				charCheck = true;
			}
		}
		return charCheck;
	}
	private boolean isEmptyOrNull(String romanNumeral) {
		return (romanNumeral == null || romanNumeral.isEmpty());
	}
	private int getIntegerValue(char singleRomanNumeral) {
		return romanMap.get(singleRomanNumeral);
	}
	private int addRomanNumeralSingleDigitsAndGroupings(String romanNumeral) {
		String romanNumeralToUpperCase = romanNumeral.toUpperCase();
		
		int value = 0;
		
		for (int i = 0; i < romanNumeralToUpperCase.length(); i++) {
			if (i + 1 < romanNumeralToUpperCase.length()) {
				if (getIntegerValue(romanNumeralToUpperCase.charAt(i)) < getIntegerValue(romanNumeralToUpperCase.charAt(i + 1))) {
					value += getIntegerValue(romanNumeralToUpperCase.charAt(i + 1)) - getIntegerValue(romanNumeralToUpperCase.charAt(i));
					i++;
				} else {
					value += getIntegerValue(romanNumeralToUpperCase.charAt(i));
				}
			} else {
				value += getIntegerValue(romanNumeralToUpperCase.charAt(i));
			}
		}
		
		return value;
	}
}
