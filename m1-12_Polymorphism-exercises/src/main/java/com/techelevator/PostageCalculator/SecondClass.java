package com.techelevator.PostageCalculator;

public class SecondClass extends PostalService {
	private String name = "Postal Service (2nd Class)";
	
	public SecondClass() {
		super(.0035, .0040, .0047, .0195, .045, .050);
	}
	public String getName() {
		return name;
	}
}
