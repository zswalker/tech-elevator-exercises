<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageTitle" value="All Customers List"/>

<%@include file="common/header.jspf"%>

<c:url var="customerResultsURL" value="/customerResults" />
<form class="customerSearchForm" action="${customerResultsURL}" method="GET">
	<input type="text" name="searchTerm" />
	<h5>Sort</h5>
	<select name="sort-by">
		<option value="last_name">Last Name</option>
		<option value="email">Email</option>
		<option value="active">Active</option>
	</select>
	<div class ="search-button">
		<input type="submit" value="Search" />
	</div>
</form>

<table class="table">
<tr>
<th>Name</th>
<th>Email</th>
<th>Active</th>
</tr>
<c:forEach items="${customerList}" var="customer">
<tr>
    <td><c:out value="${customer.firstName}" />  <c:out value="${customer.lastName}"/></td>
    <td><c:out value="${customer.email}" /></td>
    <c:choose>
    	<c:when test="${customer.active}">
    		<td>Yes</td>
    	</c:when>
    	<c:otherwise>
    		<td>No</td>
    	</c:otherwise>
    </c:choose>
</tr>
</c:forEach>
</table>

<%@include file="common/footer.jspf"%>