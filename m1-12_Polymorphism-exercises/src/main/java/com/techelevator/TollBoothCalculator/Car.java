package com.techelevator.TollBoothCalculator;

public class Car implements Tollable {
	private boolean trailerEquipped;
	
	public Car(boolean trailerEquipped) {
		this.trailerEquipped = trailerEquipped;
	}
	
	public double calculateToll(int distance) {
		if (trailerEquipped) {
			return ((distance * 0.02) + 1);
		} else {
			return (distance * 0.02);
		}
	}
	public boolean isTrailerEquipped() {
		return trailerEquipped;
	}
	@Override
	public String toString() {
		if (trailerEquipped) {
			return "Car (with trailer)"; 
		} else {
			return "Car";
		}
	}
}
