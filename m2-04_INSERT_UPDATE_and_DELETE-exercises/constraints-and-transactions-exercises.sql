-- Write queries to return the following:
-- The following changes are applied to the "dvdstore" database.**

-- 1. Add actors, Hampton Avenue, and Lisa Byway to the actor table.
INSERT INTO actor (first_name, last_name)
VALUES ('HAMPTON', 'AVENUE'), ('LISA', 'BYWAY');

SELECT * FROM actor

-- 2. Add "Euclidean PI", "The epic story of Euclid as a pizza delivery boy in
-- ancient Greece", to the film table. The movie was released in 2008 in English.
-- Since its an epic, the run length is 3hrs and 18mins. There are no special
-- features, the film speaks for itself, and doesn't need any gimmicks.
SELECT * FROM film WHERE title = 'Euclidean PI';

INSERT INTO film (title, description, release_year, language_id, length)
VALUES ('Euclidean PI', 'The epic story of Euclid as a pizza delivery boy in ancient Greece', 2008, 1, 198);

-- 3. Hampton Avenue plays Euclid, while Lisa Byway plays his slightly
-- overprotective mother, in the film, "Euclidean PI". Add them to the film.
SELECT actor_id FROM actor WHERE last_name = 'BYWAY'
SELECT actor_id FROM actor WHERE last_name = 'AVENUE'

INSERT INTO film_actor (actor_id, film_id)
VALUES (202, 1001), (201, 1001);

SELECT * FROM film_actor WHERE actor_id = 202 OR actor_id = 201;

-- 4. Add Mathmagical to the category table.
INSERT INTO category (name)
VALUES ('Mathmagical');

-- 5. Assign the Mathmagical category to the following films, "Euclidean PI",
-- "EGG IGBY", "KARATE MOON", "RANDOM GO", and "YOUNG LANGUAGE"
SELECT * FROM film WHERE title = 'KARATE MOON' OR title = 'RANDOM GO' OR title = 'YOUNG LANGUAGE' OR title = 'EGG IGBY';
                                --494                           714                     996

INSERT INTO film_category (film_id, category_id)
VALUES (1001, 17), (494, 17), (714, 17), (996, 17), (274, 17);

SELECT * FROM film_category WHERE category_id = 17;


-- 6. Mathmagical films always have a "G" rating, adjust all Mathmagical films
-- accordingly.
-- (5 rows affected)
SELECT * FROM film WHERE title = 'KARATE MOON' OR title = 'RANDOM GO' OR title = 'YOUNG LANGUAGE' OR title = 'Euclidean PI' OR title = 'EGG IGBY';
START TRANSACTION;
UPDATE film
SET rating = 'G'
WHERE film_id IN (SELECT film_id FROM film_category WHERE category_id = 17);
ROLLBACK;
COMMIT;

-- 7. Add a copy of "Euclidean PI" to all the stores.
INSERT INTO inventory (film_id, store_id)
VALUES (1001, 1), (1001, 2);


-- 8. The Feds have stepped in and have impounded all copies of the pirated film,
-- "Euclidean PI". The film has been seized from all stores, and needs to be
-- deleted from the film table. Delete "Euclidean PI" from the film table.
-- (Did it succeed? Why?)
-- It did not succeed because there is a foreign key relationship constraint.
DELETE FROM film
WHERE film.title = 'Euclidean PI';


-- 9. Delete Mathmagical from the category table.
-- (Did it succeed? Why?)
-- no, there is a foreing key constraint.
DELETE FROM category
WHERE name = 'Mathmagical';

-- 10. Delete all links to Mathmagical in the film_category tale.
-- (Did it succeed? Why?)
-- it succeeded because there is no constraint keeping it from being deleted.
DELETE FROM film_category
WHERE category_id = 17;

-- 11. Retry deleting Mathmagical from the category table, followed by retrying
-- to delete "Euclidean PI".
-- (Did either deletes succeed? Why?)
-- mathmagical was deleted because we deleted the foreign keys that are tied to it.


-- 12. Check database metadata to determine all constraints of the film id, and
-- describe any remaining adjustments needed before the film "Euclidean PI" can
-- be removed from the film table.
-- references in film_actor, film_category, and inventory would have to be deleted so no foreign keys are being used elsewhere.
SELECT * FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE
WHERE table_name = 'film';
