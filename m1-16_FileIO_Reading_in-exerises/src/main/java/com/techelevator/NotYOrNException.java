package com.techelevator;

public class NotYOrNException extends Exception{
	public NotYOrNException(String message) {
		super(message);
	}
}
