package com.techelevator.validation.model;

import java.time.LocalDate;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;


public class Registration {
	
	@NotBlank(message="First Name cannot be blank")
	@Size(min = 0, max=20, message="Dude, why is your name so long?!")
	private String firstName;
	
	@NotBlank(message="Last Name cannot be blank")
	@Size(min=0, max=20, message="Are you from the islands of Longname?")
	private String lastName;
	
	@NotBlank(message="Email cannot be blank")
	@Email(message="nice try, bucko. That wasn't a real email, was it?")
	private String email;
	
	@NotBlank(message="I do insist that you confirm your email")
	private String confirmEmail;

	
	@Size(min=8, message="Everyone knows passwords have to be at least 8 characters.")
	private String password;
	
	@NotBlank(message="No, we don't take blank passwords, so obviously this didn't match your password...")
	private String confirmPassword;
	
	@NotNull(message="tell me your birthday, going to get you a present")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private LocalDate birthday;
	
	@NotNull(message="Cannot be blank")
	@Min(value=1, message="You have to be purchasing at least one ticket or we don't want your registration")
	@Max(value=10, message="Whoa, over ten tickets? calm down...")
	private int numberOfTickets;
	
	private boolean emailConfirmed;
	@AssertTrue(message="Emails did not match")
	public boolean isEmailConfirmed() {
		if (email != null) {
			return email.equals(confirmEmail);
		} else {
			return false;
		}
	}
	
	private boolean passwordConfirmed;
	@AssertTrue(message="passwords did not match")
	public boolean isPasswordConfirmed() {
		if (password != null) {
			return password.equals(confirmPassword);
		} else {
			return false;
		}
	}
	
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getConfirmEmail() {
		return confirmEmail;
	}

	public void setConfirmEmail(String confirmEmail) {
		this.confirmEmail = confirmEmail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public int getNumberOfTickets() {
		return numberOfTickets;
	}

	public void setNumberOfTickets(int numberOfTickets) {
		this.numberOfTickets = numberOfTickets;
	}

}
