package com.techelevator;

import com.techelevator.dao.CategoryDAO;
import com.techelevator.dao.FilmDao;
import com.techelevator.dao.model.Category;
import com.techelevator.dao.model.Film;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * FilmSearchController
 */
@Controller
public class FilmSearchController {

    @Autowired
    FilmDao filmDao;
    @Autowired
    CategoryDAO categoryDao;

    @RequestMapping ("/filmSearch")
    public String showFilmSearchForm(HttpServletRequest request) {
    	
    	List<Category> categoryList = categoryDao.getCategoryList();
    	
    	request.setAttribute("genreList", categoryList);
    	
        return "filmList";
    }

    @RequestMapping ("/filmResults")
    public String searchFilms(HttpServletRequest request) {
        String genre = request.getParameter("Genre");
    	int minLength = Integer.parseInt(request.getParameter("minLength"));
    	int maxLength = Integer.parseInt(request.getParameter("maxLength"));
    	
    	List<Film> filmList = filmDao.getFilmsBetween(genre, minLength, maxLength);
    	
    	List<Category> categoryList = categoryDao.getCategoryList();
    	
    	request.setAttribute("genreList", categoryList);
    	
    	request.setAttribute("filmList", filmList);
    	
    	return "filmList";
    }
    
    
}