package com.techelevator.dao;

import com.techelevator.dao.model.Customer;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

/**
 * JDBCCustomerDao
 */
@Component
public class JDBCCustomerDao implements CustomerDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JDBCCustomerDao(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

	@Override
	public List<Customer> searchAndSortCustomers(String search, String sort) {
		
		sort = validateSort(sort);
		
		String sqlSearchAndSortCustomers = "SELECT first_name, last_name, email, active FROM customer WHERE first_name ILIKE (?) OR last_name ILIKE (?) ORDER BY " + sort;
		
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchAndSortCustomers, "%" + search + "%", "%" + search + "%");
		
		List<Customer> sortedCustomers = new ArrayList<>();
		
		while (results.next()) {
			Customer customer = mapRowToCustomer(results);
			sortedCustomers.add(customer);
		}
		
		return sortedCustomers;
	}
	
	private String validateSort(String sort) {
		
		if (sort.equals("email")) {
			sort = "email";
		} else if (sort.equals("active")) {
			sort = "active";
		} else {
			sort = "last_name";
		}
		
		return sort;
	}

	private Customer mapRowToCustomer(SqlRowSet results) {
		Customer customer = new Customer();
		
		boolean active = results.getInt("active") == 1;
		customer.setActive(active);
		customer.setEmail(results.getString("email"));
		customer.setFirstName(results.getString("first_name"));
		customer.setLastName(results.getString("last_name"));
		
		return customer;
	}
    
}