package com.techelevator.PostageCalculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PostageCalculator {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		System.out.print("Please enter the weight of the package? ");
		double weightOfPackage = in.nextDouble();
		in.nextLine();
		
		System.out.print("(P)ounds or (O)unces? ");
		String weightType = in.nextLine();
		
		double weightOfPackageInPounds = 0.0;
		double weightOfPackageInOunces = 0.0;
		
		if (weightType.equalsIgnoreCase("P")) {
			weightOfPackageInPounds = weightOfPackage;
			weightOfPackageInOunces = weightOfPackage * 16.0;
		} else if (weightType.equalsIgnoreCase("O")) {
			weightOfPackageInPounds = weightOfPackage / 16.0;
			weightOfPackageInOunces = weightOfPackage;
		}
		
		System.out.print("What distance will it be traveling in Miles? ");
		int distanceInMiles = in.nextInt();
		in.hasNextLine();
		
		System.out.println("");
		System.out.printf("%-32s $ cost", "Delivery Method");
		System.out.println("\n---------------------------------------");

		List<Deliverable> checkShippingRatesList = new ArrayList<Deliverable>();
		checkShippingRatesList.add(new FirstClass());
		checkShippingRatesList.add(new SecondClass());
		checkShippingRatesList.add(new ThirdClass());
		checkShippingRatesList.add(new FexEd());
		checkShippingRatesList.add(new FourDayGround());
		checkShippingRatesList.add(new TwoDayBusiness());
		checkShippingRatesList.add(new NextDay());
		
		for (Deliverable rate : checkShippingRatesList) {
			System.out.printf("%-32s $%-4.2f\n", rate.getName(), rate.calculateRate(distanceInMiles, weightOfPackageInOunces));
		}
		
//		List<PostalService> checkPostalServiceRateList = new ArrayList<PostalService>();
//		checkPostalServiceRateList.add(new FirstClass());
//		checkPostalServiceRateList.add(new SecondClass());
//		checkPostalServiceRateList.add(new ThirdClass());
//		
//		for (PostalService shipClass : checkPostalServiceRateList) {
//			System.out.printf("%-32s $%-4.2f\n", shipClass.getName(), shipClass.calculateRate(distanceInMiles, weightOfPackageInOunces));
//		}
//		
//		FexEd checkFexEdRate = new FexEd();
//		System.out.printf("%-32s $%-4.2f\n", checkFexEdRate.getName(), checkFexEdRate.calculateRate(distanceInMiles, weightOfPackageInOunces));
//		
//		List<SPU> checkSPURateList = new ArrayList<SPU>();
//		checkSPURateList.add(new FourDayGround());
//		checkSPURateList.add(new TwoDayBusiness());
//		checkSPURateList.add(new NextDay());
//		
//		for (SPU shipType : checkSPURateList) {
//			System.out.printf("%-32s $%-4.2f\n", shipType.getName(), shipType.calculateRate(distanceInMiles, weightOfPackageInPounds));
//		}
	}

}
