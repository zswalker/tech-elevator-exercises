package com.techelevator.PostageCalculator;

public class ThirdClass extends PostalService {
	private String name = "Postal Service (3rd Class)";
	
	public ThirdClass() {
		super(.0020, .0022, .0024, .0150, .0160, .0170);
	}
	public String getName() {
		return name;
	}
 }
