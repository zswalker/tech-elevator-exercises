package com.techelevator;

import org.junit.*;

public class StringBitsTest {
	private StringBits test;
	
	@Before
	public void setup() {
		test = new StringBits();
	}
	@Test
	public void hello_string_returns_hlo() {
		String result = test.getBits("hello");
		
		Assert.assertEquals("hlo", result);
	}
	@Test
	public void mixed_case_string_returns_mixed_case_string() {
		String result = test.getBits("heLlo");
		
		Assert.assertEquals("hLo", result);
	}
	@Test
	public void empty_string_returns_empty_string() {
		String result = test.getBits("");
		
		Assert.assertEquals("", result);
	}
	@Test
	public void null_string_returns_null() {
		String result = test.getBits(null);
		
		Assert.assertEquals(null, result);
	}
}
