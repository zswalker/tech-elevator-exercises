package com.techelevator;

public class NotValidSelectionException extends Exception {
	public NotValidSelectionException(String message) {
		super(message);
	}
}
