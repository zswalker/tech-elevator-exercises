package com.techelevator;

public class Exercises {

	public static void main(String[] args) {

        /*
        1. 4 birds are sitting on a branch. 1 flies away. How many birds are left on
        the branch?
        */

		// ### EXAMPLE:
		int initialNumberOfBirds = 4;
		int birdsThatFlewAway = 1;
		int remainingNumberOfBirds = initialNumberOfBirds - birdsThatFlewAway;

        /*
        2. There are 6 birds and 3 nests. How many more birds are there than
        nests?
        */

		// ### EXAMPLE:
		int numberOfBirds = 6;
		int numberOfNests = 3;
		int numberOfExtraBirds = numberOfBirds - numberOfNests;

        /*
        3. 3 raccoons are playing in the woods. 2 go home to eat dinner. How
        many raccoons are left in the woods?
        */
		int numberRaccoonsPlayingInWoods = 3;
		int numberRaccoonsThatGoHome = 2;
		
		int remainingRaccoons = numberRaccoonsPlayingInWoods - numberRaccoonsThatGoHome;
		System.out.println(remainingRaccoons);
        /*
        4. There are 5 flowers and 3 bees. How many less bees than flowers?
        */
		int numberOfFlowers = 5;
		int numberOfBees = 3;
		
		int differenceBetweenFlowersAndBees = numberOfFlowers - numberOfBees;
		System.out.println(differenceBetweenFlowersAndBees);
        /*
        5. 1 lonely pigeon was eating breadcrumbs. Another pigeon came to eat
        breadcrumbs, too. How many pigeons are eating breadcrumbs now?
        */
		int initialPigeonsEating = 1;
		int additionalPigeonsEating = 1;
		
		int totalPigeonsEating = initialPigeonsEating + additionalPigeonsEating;
		System.out.println(totalPigeonsEating);
        /*
        6. 3 owls were sitting on the fence. 2 more owls joined them. How many
        owls are on the fence now?
        */
		int initialOwlsSitting = 3;
		int additionalOwlsSitting = 2;
		
		int totalOwlsSitting = initialOwlsSitting + additionalOwlsSitting;
		System.out.println(totalOwlsSitting);
        /*
        7. 2 beavers were working on their home. 1 went for a swim. How many
        beavers are still working on their home?
        */
		int beaversWorking = 2;
		int beaversLeaving = 1;
		
		int totalBeaversWorking = beaversWorking - beaversLeaving;
		System.out.println(totalBeaversWorking);
        /*
        8. 2 toucans are sitting on a tree limb. 1 more toucan joins them. How
        many toucans in all?
        */
		int toucansSitting = 2;
		int toucansJoining = 1;
		
		int totalToucansSitting = toucansSitting + toucansJoining;
		System.out.println(totalToucansSitting);
        /*
        9. There are 4 squirrels in a tree with 2 nuts. How many more squirrels
        are there than nuts?
        */
		int numberOfSquirrels = 4;
		int numberOfNuts = 2;
		
		int differenceBetweenSquirrelsAndNuts = numberOfSquirrels - numberOfNuts;
		System.out.println(differenceBetweenSquirrelsAndNuts);
        /*
        10. Mrs. Hilt found a quarter, 1 dime, and 2 nickels. How much money did
        she find?
        */
		float quarterValue = .25f;
		float dimeValue  = .1f;
		float nickelValue = .05f;
		float quartersFound = 1f;
		float dimesFound = 1f;
		float nickelsFound = 2f;
		
		float totalMoneyFound = (quarterValue * quartersFound) + (dimeValue * dimesFound) + (nickelValue * nickelsFound);
		System.out.println(totalMoneyFound);
		
        /*
        11. Mrs. Hilt's favorite first grade classes are baking muffins. Mrs. Brier's
        class bakes 18 muffins, Mrs. MacAdams's class bakes 20 muffins, and
        Mrs. Flannery's class bakes 17 muffins. How many muffins does first
        grade bake in all?
        */
		int numberOfMuffinsBrierClassCooked = 18;
		int numberOfMuffinsMacadamsClassCooked = 20;
		int numberOfMuffinsFlanneryClassCooked = 17;
		
		int totalMuffinsCooked = numberOfMuffinsBrierClassCooked + numberOfMuffinsMacadamsClassCooked + numberOfMuffinsFlanneryClassCooked;
		System.out.println(totalMuffinsCooked);
        /*
        12. Mrs. Hilt bought a yoyo for 24 cents and a whistle for 14 cents. How
        much did she spend in all for the two toys?
        */
		float costOfYoyo = .24f;
		float costOfWhistle = .14f;
		
		float amountSpentOnToys = costOfYoyo + costOfWhistle;
		System.out.println(amountSpentOnToys);
        /*
        13. Mrs. Hilt made 5 Rice Krispie Treats. She used 8 large marshmallows
        and 10 mini marshmallows.How many marshmallows did she use
        altogether?
        */
		int largeMarshmallowsUsed = 8;
		int miniMarshmallowsUsed = 10;
		
		int totalMarshmallowsUsed = largeMarshmallowsUsed + miniMarshmallowsUsed;
		System.out.println(totalMarshmallowsUsed);
        /*
        14. At Mrs. Hilt's house, there was 29 inches of snow, and Brecknock
        Elementary School received 17 inches of snow. How much more snow
        did Mrs. Hilt's house have?
        */
		int inchesOfSnowAtHiltHouse = 29;
		int inchesOfSnowAtBrecknockSchool = 17;
		
		int totalInchesOfSnow = inchesOfSnowAtHiltHouse + inchesOfSnowAtBrecknockSchool;
		System.out.println(totalInchesOfSnow);
        /*
        15. Mrs. Hilt has $10. She spends $3 on a toy truck and $2 on a pencil
        case. How much money does she have left?
        */
		float initialMoney = 10f;
		float costOfToyTruck = 3f;
		float costOfPencilCase = 2f;
		
		float moneyLeft = initialMoney - costOfToyTruck - costOfPencilCase;
		System.out.println(moneyLeft);
        /*
        16. Josh had 16 marbles in his collection. He lost 7 marbles. How many
        marbles does he have now?
        */
		int initialMarbleCollection = 16;
		int amountLostMarbles = 7;
		
		int marblesRemaining = initialMarbleCollection - amountLostMarbles;
		System.out.println(marblesRemaining);
        /*
        17. Megan has 19 seashells. How many more seashells does she need to
        find to have 25 seashells in her collection?
        */
		int numberOfCurrentSeashells = 19;
		int numberOfSeashellsWanted = 25;
		
		int totalSeashellsNeeded = numberOfSeashellsWanted - numberOfCurrentSeashells;
		System.out.println(totalSeashellsNeeded);
        /*
        18. Brad has 17 balloons. 8 balloons are red and the rest are green. How
        many green balloons does Brad have?
        */
		int totalBalloons = 17;
		int numberOfRedBalloons = 8;
		
		int numberOfGreenBalloons = totalBalloons - numberOfRedBalloons;
		System.out.println(numberOfGreenBalloons);
        /*
        19. There are 38 books on the shelf. Marta put 10 more books on the shelf.
        How many books are on the shelf now?
        */
		int initialNumberOfBooksOnShelf = 38;
		int numberOfBooksAddedToShelf = 10;
		
		int totalNumberOfBooksOnShelf = initialNumberOfBooksOnShelf + numberOfBooksAddedToShelf;
		System.out.println(totalNumberOfBooksOnShelf);
        /*
        20. A bee has 6 legs. How many legs do 8 bees have?
        */
		int numberOfLegsOnABee = 6;
		int totalBees = 8;
		
		int totalBeeLegs = totalBees * numberOfLegsOnABee;
		System.out.println(totalBeeLegs);
        /*
        21. Mrs. Hilt bought an ice cream cone for 99 cents. How much would 2 ice
        cream cones cost?
        */
		float costOfIceCreamCone = .99f;
		float numberOfIceCreamCones = 2f;
		
		float totalCostOfIceCreamCones = costOfIceCreamCone * numberOfIceCreamCones;
		System.out.println(totalCostOfIceCreamCones);
        /*
        22. Mrs. Hilt wants to make a border around her garden. She needs 125
        rocks to complete the border. She has 64 rocks. How many more rocks
        does she need to complete the border?
        */
		int rocksNeededToCompleteBorder = 125;
		int currentRockCount = 64;
		
		int rocksStillNeeded = rocksNeededToCompleteBorder - currentRockCount;
		System.out.println(rocksStillNeeded);
        /*
        23. Mrs. Hilt had 38 marbles. She lost 15 of them. How many marbles does
        she have left?
        */
		int hiltMarbleAmountBegin = 38;
		int hiltMarblesLost = 15;
		
		int hiltMarblesLeft = hiltMarbleAmountBegin  - hiltMarblesLost;
		System.out.println(hiltMarblesLeft);
        /*
        24. Mrs. Hilt and her sister drove to a concert 78 miles away. They drove 32
        miles and then stopped for gas. How many miles did they have left to drive?
        */
		int totalMilesToConcert = 78;
		int milesDriven = 32;
		
		int remainingMiles = totalMilesToConcert - milesDriven;
		System.out.println(remainingMiles);
        /*
        25. Mrs. Hilt spent 1 hour and 30 minutes shoveling snow on Saturday
        morning and 45 minutes shoveling snow on Saturday afternoon. How
        much total time did she spend shoveling snow?
        */
		float morningTimeSpentShoveling = 1.5f;
		float afternoonTimeSpentShoveling = .75f;
		
		float totalTimeSpentShoveling = morningTimeSpentShoveling + afternoonTimeSpentShoveling;
		System.out.println(totalTimeSpentShoveling);
        /*
        26. Mrs. Hilt bought 6 hot dogs. Each hot dog cost 50 cents. How much
        money did she pay for all of the hot dogs?
        */
		float hotDogsBought = 6f;
		float costOfHotDogs = .5f;
		
		float totalAmountPaidForHotDogs = hotDogsBought * costOfHotDogs;
		System.out.println(totalAmountPaidForHotDogs);
        /*
        27. Mrs. Hilt has 50 cents. A pencil costs 7 cents. How many pencils can
        she buy with the money she has?
        */
		float hiltAmountOfMoney = .5f;
		float costOfPencil = .07f;
		
		int numberOfPencilsHiltCanAfford = (int)(hiltAmountOfMoney / costOfPencil);
		System.out.println(numberOfPencilsHiltCanAfford);
        /*
        28. Mrs. Hilt saw 33 butterflies. Some of the butterflies were red and others
        were orange. If 20 of the butterflies were orange, how many of them
        were red?
        */
		int totalButterfliesSeen = 33;
		int amountOfOrangeButterflies = 20;
		
		int amountOfRedButterflies = totalButterfliesSeen - amountOfOrangeButterflies;
		System.out.println(amountOfRedButterflies);
        /*
        29. Kate gave the clerk $1.00. Her candy cost 54 cents. How much change
        should Kate get back?
        */
		double moneyPaid = 1;
		double costOfCandy = .54;
		
		double changeBack = moneyPaid - costOfCandy;
		System.out.println(changeBack);
        /*
        30. Mark has 13 trees in his backyard. If he plants 12 more, how many trees
        will he have?
        */
		int initialTrees = 13;
		int additionalTreesPlanted = 12;
		
		int totalTrees = initialTrees + additionalTreesPlanted;
		System.out.println(totalTrees);
        /*
        31. Joy will see her grandma in two days. How many hours until she sees
        her?
        */
		final int NUMBER_OF_HOURS_PER_DAY = 24;
		int daysUntilJoySeesGrandma = 2;
		
		int hoursUntilJoySeesGrandma = daysUntilJoySeesGrandma * NUMBER_OF_HOURS_PER_DAY;
		System.out.println(hoursUntilJoySeesGrandma);
        /*
        32. Kim has 4 cousins. She wants to give each one 5 pieces of gum. How
        much gum will she need?
        */
		int numberOfCousins = 4;
		int piecesOfGumPerCousin = 5;
		
		int gumNeeded = numberOfCousins * piecesOfGumPerCousin;
		System.out.println(gumNeeded);
        /*
        33. Dan has $3.00. He bought a candy bar for $1.00. How much money is
        left?
        */
		float danInitialMoney = 3.0f;
		float costOfCandyBar = 1.0f;
		
		float danMoneyRemaining = danInitialMoney - costOfCandyBar;
		System.out.println(danMoneyRemaining);
        /*
        34. 5 boats are in the lake. Each boat has 3 people. How many people are
        on boats in the lake?
        */
		int numberOfBoatsInLake = 5;
		int numberOfPeoplePerBoat = 3;
		
		int totalPeopleOnBoatsInLake = numberOfBoatsInLake * numberOfPeoplePerBoat;
		System.out.println(totalPeopleOnBoatsInLake);
        /*
        35. Ellen had 380 legos, but she lost 57 of them. How many legos does she
        have now?
        */
		int initialLegoAmount = 380;
		int amountOfLegosLost = 57;
		
		int legosRemaining = initialLegoAmount - amountOfLegosLost;
		System.out.println(legosRemaining);
        /*
        36. Arthur baked 35 muffins. How many more muffins does Arthur have to
        bake to have 83 muffins?
        */
		int totalMuffinsNeeded = 83;
		int numberOfMuffinsBaked = 35;
		
		int muffinsLeftToBake = totalMuffinsNeeded - numberOfMuffinsBaked;
		System.out.println(muffinsLeftToBake);
        /*
        37. Willy has 1400 crayons. Lucy has 290 crayons. How many more
        crayons does Willy have then Lucy?
        */
		int willyCrayonCount = 1400;
		int lucyCrayonCount = 290;
		
		int differenceBetweenWillyAndLucyCrayonCount = willyCrayonCount - lucyCrayonCount;
		System.out.println(differenceBetweenWillyAndLucyCrayonCount);
        /*
        38. There are 10 stickers on a page. If you have 22 pages of stickers, how
        many stickers do you have?
        */
		int numberOfStickersPerPage = 10;
		int numberOfPages = 22;
		
		int totalStickers = numberOfStickersPerPage * numberOfPages;
		System.out.println(totalStickers);
        /*
        39. There are 96 cupcakes for 8 children to share. How much will each
        person get if they share the cupcakes equally?
        */
		float numberOfCupcakes = 96;
		float numberOfChildren = 8;
		
		float cupcakePortionPerChild = numberOfCupcakes / numberOfChildren;
		System.out.println(cupcakePortionPerChild);
        /*
        40. She made 47 gingerbread cookies which she will distribute equally in
        tiny glass jars. If each jar is to contain six cookies each, how many
        cookies will not be placed in a jar?
        */
		int totalGingerbreadCookiesMade = 47;
		int amountOfGingerbreadCookiesPerJar = 6;
		
		int gingerbreadCookiesLeftOver = totalGingerbreadCookiesMade % amountOfGingerbreadCookiesPerJar;
		System.out.println(gingerbreadCookiesLeftOver);
        /*
        41. She also prepared 59 croissants which she plans to give to her 8
        neighbors. If each neighbor received and equal number of croissants,
        how many will be left with Marian?
        */
		int numberOfCroissantsPrepared = 59;
		int numberOfNeighbors = 8;
		
		int croissantsLeftOver = numberOfCroissantsPrepared % numberOfNeighbors;
		System.out.println(croissantsLeftOver);
        /*
        42. Marian also baked oatmeal cookies for her classmates. If she can
        place 12 cookies on a tray at a time, how many trays will she need to
        prepare 276 oatmeal cookies at a time?
        */
		double cookiesPerTray = 12;
		double totalCookiesNeeded = 276;
		
		double totalTraysNeeded = Math.ceil(totalCookiesNeeded / cookiesPerTray);
		System.out.println(totalTraysNeeded);
        /*
        43. Marian’s friends were coming over that afternoon so she made 480
        bite-sized pretzels. If one serving is equal to 12 pretzels, how many
        servings of bite-sized pretzels was Marian able to prepare?
        */
		int amountOfPretzelsMade = 480;
		int numberOfPretzelsPerServing = 12;
		
		int totalServingsMade = amountOfPretzelsMade / numberOfPretzelsPerServing;
		System.out.println(totalServingsMade);
        /*
        44. Lastly, she baked 53 lemon cupcakes for the children living in the city
        orphanage. If two lemon cupcakes were left at home, how many
        boxes with 3 lemon cupcakes each were given away?
        */
		int cupcakesBaked = 53;
		int cupcakesLeftHome = 2;
		int cupcakesPerBox = 3;
		
		int total3CupcakeBoxesGiven = (cupcakesBaked - cupcakesLeftHome) / cupcakesPerBox;
		System.out.println(total3CupcakeBoxesGiven);
        /*
        45. Susie's mom prepared 74 carrot sticks for breakfast. If the carrots
        were served equally to 12 people, how many carrot sticks were left
        uneaten?
        */
		int carrotsPrepared = 74;
		int peopleEating = 12;
		
		int carrotsLeftOver = carrotsPrepared % peopleEating;
		System.out.println(carrotsLeftOver);
        /*
        46. Susie and her sister gathered all 98 of their teddy bears and placed
        them on the shelves in their bedroom. If every shelf can carry a
        maximum of 7 teddy bears, how many shelves will be filled?
        */
		int totalTeddyBears = 98;
		int teddyBearsPerShelf = 7;
		
		int totalShelvesFilled = totalTeddyBears / teddyBearsPerShelf;
		System.out.println(totalShelvesFilled);
        /*
        47. Susie’s mother collected all family pictures and wanted to place all of
        them in an album. If an album can contain 20 pictures, how many
        albums will she need if there are 480 pictures?
        */
		int totalPictures = 480;
		int picturesPerAlbum = 20;
		
		int totalAlbumsNeeded = totalPictures / picturesPerAlbum;
		System.out.println(totalAlbumsNeeded);
        /*
        48. Joe, Susie’s brother, collected all 94 trading cards scattered in his
        room and placed them in boxes. If a full box can hold a maximum of 8
        cards, how many boxes were filled and how many cards are there in
        the unfilled box?
        */
		int totalCards = 94;
		int cardsPerBox = 8;
		
		int boxesFilled = totalCards / cardsPerBox;
		System.out.println(boxesFilled);
		int cardsLeftInUnfilledBox = totalCards % cardsPerBox;
		System.out.println(cardsLeftInUnfilledBox);
        /*
        49. Susie’s father repaired the bookshelves in the reading room. If he has
        210 books to be distributed equally on the 10 shelves he repaired,
        how many books will each shelf contain?
        */
		int totalBooks = 210;
		int totalShelves = 10;
		
		int booksPerShelf = totalBooks / totalShelves;
		System.out.println(booksPerShelf);
        /*
        50. Cristina baked 17 croissants. If she planned to serve this equally to
        her seven guests, how many will each have?
        */
		float numberOfCroissantsBaked = 17;
		float numberOfGuests = 7;
		
		float croissantsPerGuest = numberOfCroissantsBaked / numberOfGuests;
		System.out.println(croissantsPerGuest);
        /*
            CHALLENGE PROBLEMS
        */

        /*
        Bill and Jill are house painters. Bill can paint a 12 x 14 room in 2.15 hours, while Jill averages
        1.90 hours. How long will it take the two painter working together to paint 5 12 x 14 rooms?
        Hint: Calculate the hourly rate for each painter, combine them, and then divide the total walls in feet by the combined hourly rate of the painters.
        Challenge: How many days will it take the pair to paint 623 rooms assuming they work 8 hours a day?.
        */
		int shortWallInFeet = 12;
		int longWallInFeet = 14;
		final int NUMBER_OF_EACH_WALL_TYPE_PER_ROOM = 2;
		
		int totalPerimeterOfRoomInFeet = (shortWallInFeet * NUMBER_OF_EACH_WALL_TYPE_PER_ROOM) + (longWallInFeet * NUMBER_OF_EACH_WALL_TYPE_PER_ROOM);
		
		float billRoomTime = 2.15f;
		float jillRoomTime = 1.9f;
		
		float billFeetOfWallPerHour = totalPerimeterOfRoomInFeet / billRoomTime;
		float jillFeetOfWallPerHour = totalPerimeterOfRoomInFeet / jillRoomTime;
		
		float bothFeetOfWallPerHour = billFeetOfWallPerHour + jillFeetOfWallPerHour;
		
		int roomsToBePainted = 5;
		
		float totalTimeToPaint5Rooms = (roomsToBePainted * totalPerimeterOfRoomInFeet) / bothFeetOfWallPerHour;
		System.out.println(totalTimeToPaint5Rooms);
		
		int hoursInWorkDay = 8;
		roomsToBePainted = 623;
		
		float totalTimeToPaint623Rooms = (roomsToBePainted * totalPerimeterOfRoomInFeet) / bothFeetOfWallPerHour;
		
		double totalDaysToPaint623Rooms = Math.ceil(totalTimeToPaint623Rooms / hoursInWorkDay);
		System.out.println(totalDaysToPaint623Rooms);
		
        /*
        Create and assign variables to hold your first name, last name, and middle initial. Using concatenation,
        build an additional variable to hold your full name in the order of last name, first name, middle initial. The
        last and first names should be separated by a comma followed by a space, and the middle initial must end
        with a period.
        Example: "Hopper, Grace B."
        */
		String firstName = "Zach";
		String lastName = "Walker";
		String middleInitial = "S";
		
		String fullName = lastName + ", " + firstName + " " + middleInitial + ".";
		System.out.println(fullName);
        /*
        The distance between New York and Chicago is 800 miles, and the train has already travelled 537 miles.
        What percentage of the trip has been completed?
        Hint: The percent completed is the miles already travelled divided by the total miles.
        Challenge: Display as an integer value between 0 and 100 using casts.
        */
		double totalMiles = 800;
		double milesTraveled = 537;
		
		double percentTripCompleted = milesTraveled /  totalMiles;
		System.out.println(percentTripCompleted);
		int percentTripCompletedAsWholeNumber = (int)(percentTripCompleted * 100);
		System.out.println(percentTripCompletedAsWholeNumber);

	}

}
