<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="pageTitle" value="Login"/>
<%@include file="common/header.jspf" %>

<h1>Login</h1>

<c:url value="/login" var="loginLink"/>
<form:form action="${loginLink}" method="POST" modelAttribute="login">

	<div>
	<label for="email">Email</label>
		<form:input type ="email" path="email" placeholder="enter email"/>
      		<form:errors path="email" cssClass="error"/>
	</div>
	
	
	<div>
	<label for="password">Password</label>
	<form:input type ="password" path="password" placeholder="enter password"/>
      		<form:errors path="password" cssClass="error"/>
	</div>
	
	<button>Submit</button>

</form:form>

<%@include file="common/footer.jspf" %>