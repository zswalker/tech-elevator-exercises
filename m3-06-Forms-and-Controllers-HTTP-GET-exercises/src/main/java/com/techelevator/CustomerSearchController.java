package com.techelevator;

import com.techelevator.dao.CustomerDao;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CustomerSearchController {

    @Autowired
    private CustomerDao customerDao;
    
    @RequestMapping("/customerSearch")
    public String displayCustomerSearch() {
    	return "customerList";
    }
    
    @RequestMapping("/customerResults")
    public String displayCustomerResults(HttpServletRequest request) {
    	
    	String search = request.getParameter("searchTerm");
    	String sort = request.getParameter("sort-by");
    	
    	request.setAttribute("customerList", customerDao.searchAndSortCustomers(search, sort));
    	
    	return "customerList";
    }
    

}