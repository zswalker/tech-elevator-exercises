package com.techelevator;

import org.junit.*;

public class Less20Test {
	private Less20 test;
	
	@Before
	public void setup() {
		test = new Less20();
	}
	@Test
	public void n_one_less_than_modulus_20_return_true() {
		boolean result = test.isLessThanMultipleOf20(19);
		
		Assert.assertTrue(result);
	}
	@Test
	public void n_two_less_than_modulus_20_return_true() {
		boolean result = test.isLessThanMultipleOf20(18);
		
		Assert.assertTrue(result);
	}
	@Test
	public void negative_n_returns_false() {
		boolean result = test.isLessThanMultipleOf20(-10);
		
		Assert.assertFalse(result);
	}
	@Test
	public void zero_n_returns_false() {
		boolean result = test.isLessThanMultipleOf20(0);
		
		Assert.assertFalse(result);
	}
}
