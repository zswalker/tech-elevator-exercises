package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class FizzWriter {

	public static void main(String[] args) throws IOException, FileNotFoundException {
		File file = new File("FizzBuzz.txt");

		writeFizzBuzzFile(file);

		confirmFileCreation(file);

	}
	private static void writeFizzBuzzFile(File file) throws FileNotFoundException {
		try (PrintWriter fileWriter = new PrintWriter(file)) {
			for (int i = 1; i <= 300; i++) {
				if (i % 3 == 0 && i % 5 == 0) {
					fileWriter.println("FizzBuzz");
				} else if (i % 3 == 0) {
					fileWriter.println("Fizz");
				} else if (i % 5 == 0) {
					fileWriter.println("Buzz");
				} else {
					fileWriter.println(i);
				}
			}
		}
	}
	private static void confirmFileCreation(File file) {
		if (file.exists()) {
			System.out.println(file.getName() + " has been created.");
		} else {
			System.out.println("error file not created");
		}
	}

}
