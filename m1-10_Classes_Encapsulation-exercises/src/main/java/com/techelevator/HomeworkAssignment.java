package com.techelevator;

public class HomeworkAssignment {

	private int totalMarks;
	private int possibleMarks;
	private String submitterName;
	
	public HomeworkAssignment(int possibleMarks) {
		this.possibleMarks = Math.abs(possibleMarks);
	}
	
	public String getLetterGrade() {
		if (calculatePercent() >= 90) {
			return "A";
		}
		if (calculatePercent() >= 80) {
			return "B";
		}
		if (calculatePercent() >= 70) {
			return "C";
		}
		if (calculatePercent() >= 60) {
			return "D";
		}
		
		return "F";
	}
	
	private double calculatePercent() {
		double totalMarksAsDouble = (double)totalMarks;
		double possibleMarksAsDouble = (double)possibleMarks;
		
		return (totalMarksAsDouble / possibleMarksAsDouble) * 100;
	}
	
	public int getTotalMarks() {
		return totalMarks;
	}
	
	public void setTotalMarks(int totalMarks) {
		this.totalMarks = totalMarks;
	}
	
	public int getPossibleMarks() {
		return possibleMarks;
	}
	
	public String getSubmitterName() {
		return submitterName;
	}
	
	public void setSubmitterName(String submitterName) {
		this.submitterName = submitterName;
	}
	
}
