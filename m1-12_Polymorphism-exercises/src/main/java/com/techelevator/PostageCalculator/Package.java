package com.techelevator.PostageCalculator;

public class Package {
	
	//ended up not using this class in exercise
	
	private double weight;
	private boolean isWeightInPounds;
	
	public Package(int weight, boolean isWeightInPounds) {
		this.weight = Math.abs(weight);
		this.isWeightInPounds = isWeightInPounds;
	}
	
	public boolean isWeightInPounds() {
		return isWeightInPounds;
	}
	public double getWeightInOunces() {
		if (isWeightInPounds()) {
		return weight * 16;
		} else {
			return weight;
		}
	}
	public double getWeight() {
		return weight;
	}
}
