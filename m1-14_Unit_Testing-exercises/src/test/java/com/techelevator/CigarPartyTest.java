package com.techelevator;

import org.junit.*;

public class CigarPartyTest {
	private CigarParty test;
	
	@Before
	public void setup() {
		test = new CigarParty();
	}
	@Test
	public void cigars_less_than_40_and_is_weekend_returns_false() {
		boolean isSuccess = test.haveParty(30, true);
		
		Assert.assertFalse(isSuccess);
	}
	@Test
	public void cigars_less_than_40_and_is_not_weekend_return_false() {
		boolean isSuccess = test.haveParty(30, false);
		
		Assert.assertFalse(isSuccess);
	}
	@Test
	public void cigars_greater_than_40_less_than_60_is_not_weekend_return_true() {
		boolean  isSuccess = test.haveParty(50, false);
		
		Assert.assertTrue(isSuccess);
	}
	@Test
	public void cigars_greater_than_40_is_weekend_return_true() {
		boolean isSuccess = test.haveParty(70, true);
		
		Assert.assertTrue(isSuccess);
	}
	@Test
	public void cigars_is_negative_return_false() {
		boolean isSuccess = test.haveParty(-10, true);
		
		Assert.assertFalse(isSuccess);
	}
	@Test
	public void cigars_greater_than_60_is_not_weekend_return_false() {
		boolean isSuccess = test.haveParty(70, false);
		
		Assert.assertFalse(isSuccess);
	}
	@Test
	public void cigars_at_40_is_weekend_return_true() {
		boolean isSuccess = test.haveParty(40, true);
		
		Assert.assertTrue(isSuccess);
	}
	@Test
	public void cigars_at_40_is_not_weekend_return_true() {
		boolean isSuccess = test.haveParty(40, false);
		
		Assert.assertTrue(isSuccess);
	}
	
}
