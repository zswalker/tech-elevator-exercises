package com.techelevator.TollBoothCalculator;

public class Truck implements Tollable {
	private int numberOfAxles;
	
	public Truck(int numberOfAxles) {
		this.numberOfAxles = numberOfAxles;
	}
	public double calculateToll(int distance) {
		if (numberOfAxles == 4) {
			return distance * .04;
		} else if (numberOfAxles == 6) {
			return distance * .045;
		} else {
			return distance * .048;
		}
	}
	public int getNumberOfAxles() {
		return numberOfAxles;
	}
	@Override
	public String toString() {
		return "Truck (with " + numberOfAxles + " axles)";
	}
}
