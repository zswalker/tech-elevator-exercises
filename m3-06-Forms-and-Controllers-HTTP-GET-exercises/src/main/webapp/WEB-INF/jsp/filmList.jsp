<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pageTitle" value="All Films List"/>

<%@include file="common/header.jspf"%>

<c:url var="filmResultsURL" value="/filmResults" />
<form class="filmSearchForm" action="${filmResultsURL}" method="GET">
	<h5>Minimum Length:</h5>
	<input type="number" name="minLength" />
	<h5>Maximum Length:</h5>
	<input type="number" name="maxLength"/>
	
	<h5>Genre:</h5>
	<select name="Genre">
	<c:forEach var="genre" items="${genreList}">
		<option value="${genre.name}">${genre.name}</option>
	</c:forEach>
	</select>
	<div class ="search-button">
		<input type="submit" value="Search" />
	</div>
</form>

<table class="table">
<tr>
<th>Title</th>
<th>Description</th>
<th>Release Year</th>
<th>Length</th>
<th>Rating</th>
</tr>
<c:forEach items="${filmList}" var="film">
<tr>
    <td><c:out value="${film.title}" /></td>
    <td><c:out value="${film.description}" /></td>
    <td><c:out value="${film.releaseYear}" /></td>
    <td><c:out value="${film.length}" /></td>
    <td><c:out value="${film.rating}" /></td>
</tr>
</c:forEach>
</table>

<%@include file="common/footer.jspf"%>