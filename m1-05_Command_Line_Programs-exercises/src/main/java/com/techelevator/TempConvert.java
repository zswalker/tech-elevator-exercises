package com.techelevator;

import java.util.Scanner;

public class TempConvert {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		
		System.out.print("Please enter the temperature: ");
		double userTemp = in.nextDouble();
		
		System.out.print("Is the temperature in (C)elsius or (F)ahrenheit?: ");
		String userTempType = in.next();
		
		double newTemp = 0;
		
		if (userTempType.equals("f") || userTempType.equals("F")) {
			newTemp = (userTemp - 32) / 1.8;
		} else if (userTempType.equals("c") || userTempType.equals("C")) {
			newTemp = (userTemp * 1.8 + 32);
		} else {
		System.out.print("ERROR: invalid temp type");
		}
		
		String oppTempType = "";
		
		if (userTempType.equals("F") || userTempType.equals("f")) {
			oppTempType = "C";
		} else if (userTempType.equals("C") || userTempType.equals("c")) {
			oppTempType = "F";
		} else {
			System.out.println("Invalid temp type");
		}
		
		System.out.print(userTemp + userTempType);
		System.out.print(" is ");
		System.out.printf("%6.2f", newTemp);
		System.out.print(oppTempType + ".");
		
	}

}
