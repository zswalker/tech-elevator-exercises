package com.techelevator;

import org.junit.*;

public class Lucky13Test {
	private Lucky13 test;
	
	@Before
	public void setup() {
		test = new Lucky13();
	}
	@Test
	public void no_3_in_array_returns_true() {
		int[] array = {4, 5, 6};
		boolean result = test.getLucky(array);
		Assert.assertTrue(result);
	}
	@Test
	public void three_in_array_returns_false() {
		int[] array = {3, 5, 6};
		boolean result = test.getLucky(array);
		Assert.assertFalse(result);
	}
	@Test
	public void one_in_array_returns_false() {
		int[] array = {1, 5, 6};
		boolean result = test.getLucky(array);
		Assert.assertFalse(result);
	}
	@Test
	public void negative_non_three_and_one_in_array_returns_true() {
		int[] array = {-4, -5, -6};
		boolean result = test.getLucky(array);
		Assert.assertTrue(result);
	}
	@Test
	public void negative_3_in_array_returns_true() {
		int[] array = {-3, 5, 6};
		boolean result = test.getLucky(array);
		Assert.assertTrue(result);
	}
	@Test
	public void negative_1_in_array_returns_true() {
		int[] array = {-1, 5, 6};
		boolean result = test.getLucky(array);
		Assert.assertTrue(result);
	}
}
