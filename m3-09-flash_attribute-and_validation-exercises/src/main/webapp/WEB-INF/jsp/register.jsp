<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:set var="pageTitle" value="New User"/>
<%@include file="common/header.jspf" %>

<h1>New User Registration</h1>


<c:url value="/register" var="registerLink"/>
<form:form action="${registerLink}" method="POST" modelAttribute="registration">

	<div>
	<label for="firstName">First Name</label>
		<form:input type ="text" path="firstName" placeholder="enter first name"/>
      		<form:errors path="firstName" cssClass="error"/>
	</div>
	
	
	<div>
	<label for="lastName">Last Name</label>
	<form:input type ="text" path="lastName" placeholder="enter last name"/>
      		<form:errors path="lastName" cssClass="error"/>
	</div>


	<div>
	<label for="email">Email</label>
	<form:input type ="email" path="email" placeholder="enter email"/>
      		<form:errors path="email" cssClass="error"/>
	</div>
	
	<div>
	<label for="confirmEmail">Confirm Email</label>
	<form:input type ="email" path="confirmEmail" placeholder="confirm email"/>
      		<form:errors path="confirmEmail" cssClass="error"/>
      		<form:errors path="emailConfirmed" cssClass="error"/>
	</div>
	
	<div>
	<label for="password">Password</label>
	<form:input type ="password" path="password" placeholder="enter password"/>
      		<form:errors path="password" cssClass="error"/>
      </div>
	
	<div>
	<label for="confirmPassword">Confirm Password</label>
	<form:input type ="password" path="confirmPassword" placeholder="confirm password"/>
      		<form:errors path="confirmPassword" cssClass="error"/>
      		<form:errors path="passwordConfirmed" cssClass="error"/>
	</div>
	
	<div>
	<label for="birthday">Birthday</label>
	<form:input type ="date" path="birthday" placeholder="enter birthday"/>
      		<form:errors path="birthday" cssClass="error"/>
	</div>
	
	<div>
	<label for="numberOfTickets"># of Tickets</label>
	<form:input type ="number" path="numberOfTickets" placeholder="enter number of tickets"/>
      		<form:errors path="numberOfTickets" cssClass="error"/>
	</div>
	
	<button>Submit</button>

</form:form>

<%@include file="common/footer.jspf" %>