<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/common/header.jsp"/>

<c:url value="/page1" var="page1Link"/>
<div class="favContainer">
	<h3>Favorite Color: </h3> <span><c:out value="${favThing.favoriteColor}"/></span>
</div>
<div class="favContainer">
	<h3>Favorite Food: </h3> <span><c:out value="${favThing.favoriteFood}"/></span>
</div>
<div class="favContainer">
	<h3>Favorite Season: </h3> <span><c:out value="${favThing.favoriteSeason}"/></span>
</div>

<a href="${page1Link}">Back to Question 1</a>

<c:import url="/common/footer.jsp"/>  