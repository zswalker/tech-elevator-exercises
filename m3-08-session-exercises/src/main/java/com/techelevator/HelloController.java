package com.techelevator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller 
@SessionAttributes("favoriteThing")
public class HelloController {
	
	FavoriteThing favoriteThing;
	

	@RequestMapping(path="/page1", method=RequestMethod.GET)
	public String displayQuestion1() {
		
		return "page1";
	}
	
	@RequestMapping(path="/page1", method=RequestMethod.POST)
	public String redirectToQuestion2 (@RequestParam String favColor) {
		
		favoriteThing = new FavoriteThing();
		favoriteThing.setFavoriteColor(favColor);
		
		
		return "redirect:/page2";
	}
	
	@RequestMapping(path="/page2", method=RequestMethod.GET)
	public String displayQuestion2() {
		return "page2";
	}
	
	@RequestMapping(path="/page2", method=RequestMethod.POST)
	public String redirectToQuestion3 (@RequestParam String favFood) {
	
		favoriteThing.setFavoriteFood(favFood);
		
		return "redirect:/page3";
	}
	
	@RequestMapping(path="/page3", method=RequestMethod.GET)
	public String displayQuestion3() {
		
		return "page3";
	}
	
	@RequestMapping(path="/page3", method=RequestMethod.POST)
	public String redirectToSummary(@RequestParam String favoriteSeason, ModelMap map) {
		favoriteThing.setFavoriteSeason(favoriteSeason);
		
		map.addAttribute("favThing", favoriteThing);
		
		return "summary";
	}
	
	@RequestMapping(path="/summary", method=RequestMethod.GET)
	public String displaySummary(ModelMap map) {
		
		
		
		return "summary";
	}
}
