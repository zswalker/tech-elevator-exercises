package com.techelevator.calculator;

public class Calculator {

	private int currentValue = 0;
	
	public int getCurrentValue() {
		return currentValue;
	}
	
	public int add(int addend) {
		return currentValue += addend;
	}
	
	public int subtract(int subtrahend) {
		return currentValue -= subtrahend;
	}
	
	public int multiply(int multiplier) {
		return currentValue *= multiplier;
	}
	
	public int power(int exponent) {
		if (exponent == 0) {
			return currentValue = 0;
		}
		if (exponent == 1) {
			return currentValue;
		}
		
		int powerOfInt = currentValue;
		
		for (int i = 0; i < exponent - 1; i++) {
			currentValue *= powerOfInt;
		}
		return currentValue;
	}
	
	public void reset() {
		currentValue = 0;
	}
	
}
