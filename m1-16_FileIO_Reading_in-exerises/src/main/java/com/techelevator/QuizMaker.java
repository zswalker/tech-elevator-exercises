package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class QuizMaker {

	public static void main(String[] args) throws FileNotFoundException, NotValidSelectionException {
		File inputFile = getFileFromUser();
		
		while(true) {
			try {
				getQuiz(inputFile);
			} catch (NotValidSelectionException e) {
				System.out.println("You did not enter a valid choice, start over");
				continue;
			} catch (InputMismatchException e) {
				System.out.println("You did not enter a valid choice, start over");
				continue;
			}
		}

	}
	private static File getFileFromUser() {
		Scanner userInput = new Scanner(System.in);
		System.out.println("Where is the quiz file?");
		String path = userInput.nextLine();
		
		File inputFile = new File(path);
		if (!inputFile.exists()) {
			System.out.println(path + " does not exist");
			System.exit(1);
		} else if (!inputFile.isFile()) {
			System.out.println(path + " is not a file");
			System.exit(1);
		}
		return inputFile;
	}
	private static void getQuiz(File inputFile) throws FileNotFoundException, NotValidSelectionException {
		try (Scanner fileIn = new Scanner(inputFile)) {
			int questionCount = 0;
			int correctAnswerCount = 0;
			
			while (fileIn.hasNextLine()) {
				String line = fileIn.nextLine();
				String[] quizItems = line.split("\\|");
				int multipleChoiceCount = 1;
				int correctAnswerNumber = 0;

				
				for (String quizItem : quizItems) {
					
					if (quizItems[0].equals(quizItem)) {
						System.out.println(quizItem);
						questionCount++;
					} else if (quizItem.contains("*")) {
						System.out.println("  " + multipleChoiceCount + ") " + quizItem.substring(0, quizItem.length() - 1));
						correctAnswerNumber = multipleChoiceCount;
						multipleChoiceCount++;
					} else {
						System.out.println("  " + multipleChoiceCount + ") " + quizItem);
						multipleChoiceCount++;
					}
					if (multipleChoiceCount == 5) {
						System.out.print("\nYour answer: ");
						Scanner userInput = new Scanner(System.in);
						int answer = userInput.nextInt();
						userInput.nextLine();
							if (answer == correctAnswerNumber) {
								System.out.println("Correct!\n");
								correctAnswerCount++;
							} else if (answer <= 0 || answer > 4) {
								throw new NotValidSelectionException("User entered outside of the range 1 to 4 inclusive");
							} else {
								System.out.println("Sorry that isn't correct.\n");
							}
						multipleChoiceCount = 1;
						}

				}
			}
			System.out.println("You got " + correctAnswerCount + " answer(s) correct out of the total " + questionCount + " questions asked.");
			System.exit(1);
		}
	}
}
