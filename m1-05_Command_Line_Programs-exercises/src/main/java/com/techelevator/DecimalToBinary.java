package com.techelevator;

import java.util.Scanner;

public class DecimalToBinary {

	public static void main(String[] args) {

		Scanner in = new Scanner(System.in);
		
		System.out.print("Please enter in a series of decimal values (separated by spaces):");
		String userBase10NumbersString = in.nextLine();
	
		String[] userBase10NumbersStringArray = userBase10NumbersString.split(" ", 0);
		
		for (int i = 0; i <userBase10NumbersStringArray.length; i++) {
			int userBase10Number = (int)(Double.parseDouble(userBase10NumbersStringArray[i]));
			
			System.out.print(userBase10Number + " in binary is ");
		
			int highestDivisiblePowerOf2 = 1; //started at 1 because that is lowest non-zero integer power of 2
			int nextPowerOf2GreaterThanUserNumber = 1; //started at 1 because that is lowest non-zero integer power of 2
			final int INCREMENT_NUMBER = 2; //set as constant to multiply to increment power of 2
			final int DECREMENT_NUMBER = 2; //set as constant to divide to decrement power of 2
			final int LOWEST_POWER_OF_2 = 1; //set for lowest non-zero integer power of 2
	
			
			if (userBase10Number < 0) {
				System.out.print("-"); //to insert the negative before the binary is printed
				userBase10Number *= -1; //to make number positive and fit into the rest of the calculations
			}
			
			if (userBase10Number == 0) {
				System.out.print(0);
			}
			
			while(nextPowerOf2GreaterThanUserNumber <= userBase10Number) {
					nextPowerOf2GreaterThanUserNumber *= INCREMENT_NUMBER;
			}
			highestDivisiblePowerOf2 = nextPowerOf2GreaterThanUserNumber / DECREMENT_NUMBER;
			
			int decrementedPowerOf2 = highestDivisiblePowerOf2; //decrements starting from highestDivisiblePowerOf2
			
			while (userBase10Number != 0) {
				if(userBase10Number >= decrementedPowerOf2) {
					System.out.print(userBase10Number/decrementedPowerOf2);
					userBase10Number -= decrementedPowerOf2;
					decrementedPowerOf2 /= DECREMENT_NUMBER;
				} else if (userBase10Number < decrementedPowerOf2) {
					System.out.print(0);
					decrementedPowerOf2 /= DECREMENT_NUMBER;
				}
			}
			
			while (decrementedPowerOf2 >= LOWEST_POWER_OF_2) {
				System.out.print(0);
				decrementedPowerOf2 /= DECREMENT_NUMBER;
			}
			System.out.println("");
			
			
		}
	}

}
