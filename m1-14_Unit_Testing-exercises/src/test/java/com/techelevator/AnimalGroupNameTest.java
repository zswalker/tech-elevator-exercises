package com.techelevator;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public class AnimalGroupNameTest {
	
	private AnimalGroupName target;
	
	@Before
	public void setup() {
		target = new AnimalGroupName();
	}
	@Test
	public void valid_key_returns_valid_group_name() {
		//Arrange
		//Act
		String herd = target.getHerd("lion");
		//Assert
		Assert.assertEquals("Pride", herd);
	}
	@Test
	public void correct_name_returned_when_key_is_mixed_case() {
		//Arrange
		//Act
		String herd = target.getHerd("ElePhAnT");
		//Assert
		Assert.assertEquals("Herd", herd);
	}
	@Test
	public void key_not_known_returns_unknown() {
		//Arrange
		//Act
		String herd = target.getHerd("EleAnT");
		//Assert
		Assert.assertEquals("unknown", herd);
	}
	@Test
	public void key_null_returns_unknown() {
		//Arrange
		//Act
		String herd = target.getHerd(null);
		//Assert
		Assert.assertEquals("unknown", herd);
	}
	@Test
	public void key_empty_string_returns_unknown() {
		//Arrange
		//Act
		String herd = target.getHerd("");
		//Assert
		Assert.assertEquals("unknown", herd);
	}
}
