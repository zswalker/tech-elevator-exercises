package com.techelevator;

import org.junit.*;

public class NonStartTest {
	private NonStart test;
	
	@Before
	public void setup() {
		test = new NonStart();
	}
	@Test
	public void both_strings_two_chars_return_second_letter_of_each() {
		String result = test.getPartialString("xa", "xb");
		
		Assert.assertEquals("ab", result);
	}
	@Test
	public void both_strings_one_char_return_empty_string() {
		String result = test.getPartialString("x", "x");
		
		Assert.assertEquals("", result);
	}
	@Test
	public void one_null_returns_opposite_string_minus_first_letter() {
		String result = test.getPartialString(null, "xb");
		
		Assert.assertEquals("b", result);
	}
	@Test
	public void both_null_returns_null() {
		String result = test.getPartialString(null, null);
		
		Assert.assertEquals(null, result);
	}
	@Test
	public void second_string_null_return_first_without_first_letter() {
		String result = test.getPartialString("xa", null);
		
		Assert.assertEquals("a", result);
	}
	@Test
	public void one_string_empty_return_other_string_minus_first_letter() {
		String result = test.getPartialString("", "ab");
		
		Assert.assertEquals("b", result);
	}
	@Test
	public void both_empty_string_return_empty_string() {
		String result = test.getPartialString("", "");
		
		Assert.assertEquals("", result);
	}
	@Test
	public void second_string_empty_return_first_string_minus_first_letter() {
		String result = test.getPartialString("xa", "");
		
		Assert.assertEquals("a", result);
	}
}
