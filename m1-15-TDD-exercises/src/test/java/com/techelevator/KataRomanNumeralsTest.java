package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataRomanNumeralsTest {
	private KataRomanNumerals target;
	
	@Before
	public void setup() {
		target = new KataRomanNumerals();
	}
	@Test
	public void convert_single_characters() {
		Assert.assertEquals("Failed to convert I", 1,  target.convertToDigit("I"));
		Assert.assertEquals("Failed to convert V", 5,  target.convertToDigit("V"));
		Assert.assertEquals("Failed to convert X", 10,  target.convertToDigit("X"));
	}
	@Test
	public void convert_triples_of_single_characters() {
		Assert.assertEquals("Failed to convert III", 3,  target.convertToDigit("III"));
		Assert.assertEquals("Failed to convert XXX", 30,  target.convertToDigit("XXX"));
		Assert.assertEquals("Failed to convert XVI", 16,  target.convertToDigit("XVI"));
	}
	@Test
	public void convert_roman_numeral_groupings() {
		Assert.assertEquals("Failed to convert IV", 4, target.convertToDigit("IV"));
		Assert.assertEquals("Failed to convert XL", 40, target.convertToDigit("XL"));
		Assert.assertEquals("Failed to convert XC", 90, target.convertToDigit("XC"));
		Assert.assertEquals("Failed to convert XIV", 14, target.convertToDigit("XIV"));
		Assert.assertEquals("Failed to convert XLIV", 44, target.convertToDigit("XLIV"));
		Assert.assertEquals("Failed to convert MCXLVI", 646, target.convertToDigit("DCXLVI"));
	}
	@Test
	public void converts_mixed_case() {
		Assert.assertEquals("Failed to convert McXlVi", 646, target.convertToDigit("DcXlVi"));
	}
	@Test
	public void null_string_returns_0() {
		Assert.assertEquals("Failed to return 0 for null string", 0, target.convertToDigit(null));
	}
	@Test
	public void empty_string_returns_0() {
		Assert.assertEquals("Failed to return 0 for empty string", 0, target.convertToDigit(""));
	}
	@Test
	public void convert_large_roman_numeral() {
		Assert.assertEquals(5664, target.convertToDigit("MMMMMDCLXIV"));
	}
	@Test
	public void invalid_key_returns_0() {
		Assert.assertEquals(0, target.convertToDigit("s"));
		Assert.assertEquals(0, target.convertToDigit("XwL"));
	}
	@Test
	public void number_matches_key_returns_mapped_value() {
		Assert.assertEquals("I", target.convertToRomanNumeral(1));
		Assert.assertEquals("V", target.convertToRomanNumeral(5));
		Assert.assertEquals("X", target.convertToRomanNumeral(10));
		Assert.assertEquals("M", target.convertToRomanNumeral(1000));
	}
	@Test
	public void three_returns_III() {
		Assert.assertEquals("III", target.convertToRomanNumeral(3));
	}
	@Test
	public void four_returns_IV() {
		Assert.assertEquals("IV", target.convertToRomanNumeral(4));
	}
	@Test
	public void six_through_8_returns_valid_roman_numeral() {
		Assert.assertEquals("VI", target.convertToRomanNumeral(6));
		Assert.assertEquals("VII", target.convertToRomanNumeral(7));
		Assert.assertEquals("VIII", target.convertToRomanNumeral(8));
	}
	@Test
	public void nine_through_18_return_valid_roman_numeral() {
		Assert.assertEquals("IX", target.convertToRomanNumeral(9));
		Assert.assertEquals("X", target.convertToRomanNumeral(10));
		Assert.assertEquals("XI", target.convertToRomanNumeral(11));
		Assert.assertEquals("XII", target.convertToRomanNumeral(12));
		Assert.assertEquals("XIII", target.convertToRomanNumeral(13));
		Assert.assertEquals("XIV", target.convertToRomanNumeral(14));
		Assert.assertEquals("XV", target.convertToRomanNumeral(15));
		Assert.assertEquals("XVI", target.convertToRomanNumeral(16));
		Assert.assertEquals("XVII", target.convertToRomanNumeral(17));
		Assert.assertEquals("XVIII", target.convertToRomanNumeral(18));
	}
	@Test
	public void numbers_18_through_39_return_valid_roman_numeral() {
		Assert.assertEquals("XIX", target.convertToRomanNumeral(19));
		Assert.assertEquals("XXIII", target.convertToRomanNumeral(23));
		Assert.assertEquals("XXXIX", target.convertToRomanNumeral(39));
	}
	@Test
	public void numers_40_through_49_return_valid_roman_numeral() {
		Assert.assertEquals("XL", target.convertToRomanNumeral(40));
		Assert.assertEquals("XLIX", target.convertToRomanNumeral(49));
	}
	@Test
	public void numbers_50_through_89_returun_valid_roman_numeral() {
		Assert.assertEquals("LI", target.convertToRomanNumeral(51));
		Assert.assertEquals("LXXV", target.convertToRomanNumeral(75));
		Assert.assertEquals("LXXXIX", target.convertToRomanNumeral(89));
	}
	@Test
	public void numbers_90_through_99_return_valid_roman_numerals() {
		Assert.assertEquals("XCI", target.convertToRomanNumeral(91));
		Assert.assertEquals("XCVIII", target.convertToRomanNumeral(98));
	}
	@Test
	public void numbers_100_through_399_return_valid_roman_numerals() {
		Assert.assertEquals("CX", target.convertToRomanNumeral(110));
		Assert.assertEquals("CCCLXXXIV", target.convertToRomanNumeral(384));
	}
	@Test
	public void numbers_400_through_499_return_valid_roman_nuemrals() {
		Assert.assertEquals("CDXXXII", target.convertToRomanNumeral(432));
		Assert.assertEquals("CDLXVII", target.convertToRomanNumeral(467));	
	}
	@Test
	public void numbers_500_through_899_return_valid_roman_numerals() {
		Assert.assertEquals("DLXII", target.convertToRomanNumeral(562));
		Assert.assertEquals("DXCIII", target.convertToRomanNumeral(593));
	}
	@Test
	public void numbers_900_through_999_return_valid_roman_numerals() {
		Assert.assertEquals("CMXIV", target.convertToRomanNumeral(914));
		Assert.assertEquals("CMXCVIII", target.convertToRomanNumeral(998));
	}
	@Test
	public void numbers_1000_and_greater_return_valid_roman_numerals() {
		Assert.assertEquals("MCCXXXIV", target.convertToRomanNumeral(1234));
		Assert.assertEquals("MMDCCCXLI", target.convertToRomanNumeral(2841));
		Assert.assertEquals("MMM", target.convertToRomanNumeral(3000));
		Assert.assertEquals("MMMMMDCCCXIX", target.convertToRomanNumeral(5819));
	}
	@Test
	public void negative_and_0_integers_return_invalid() {
		Assert.assertEquals("invalid", target.convertToRomanNumeral(-1));
	}
}
