<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 



<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
		<title>Hello Spring MVC</title>
		<c:url var="style" value="/css/squirrel.css"/>
		<link rel="stylesheet" href="${style}">
	</head>
	<body>
		
	<ul>
	  <li><a href="">Home</a></li>
	  <li><a href="">News</a></li>
	  <li><a href="">Contact</a></li>
	  <li><a href="">About</a></li>
	</ul>
	
		<h1 class="pageTitle">Squirrel Party Forum</h1>
		<h3 class="pageTitle">NO MONKEY BUSINESS</h3>
		
		<c:url value="/img/forDummies.png" var="bookCoverLink"/>
		<img src="${bookCoverLink}"/>
		
		<h2>About this book:</h2>
		<div class ="aboutContainer">
			<p>This book will help you bring all the squirrels to the yard</p>
			<p>And they're like, </p>
			<p>its better than yours.</p>
			<p>D*mn right, its better than yours,</p>
			<p>Craig Castelaz can teach you,</p>
			<p>but he has to charge ($49.99 for the book).</p>
		</div>
		
		<h2>Read reviews from previous Squirrel Party readers</h2>
		
		<c:forEach items="${allReviews}" var="review">
			<div class="reviewBox">
				<div id="titleAndUsernameContainer">
					<h3><c:out value="${review.title}"/></h3>
					<p><c:out value="(${review.username})"/></p>
				</div>
				<fmt:parseDate var="parsedDate" pattern="yyyy-MM-dd'T'HH:mm:ss" value="${review.dateSubmitted}"/> 
				<fmt:formatDate var="d" type="date" pattern="MM/dd/yyyy" value="${parsedDate}"/>
				<p><c:out value="${d}"/></p>
				<c:url value="/img/star.png" var="starLink"/>
				<div class="starBox">
					<c:forEach begin="1" end="${review.rating}">
						<img src="${starLink}"/>
					</c:forEach>
				</div>
				<p class="reviewContainer"><c:out value="${review.text}"/></p>
			</div>
		</c:forEach>
		
		
		<c:url value="/postReview" var="postReviewLink"/>
		<form id="reviewForm" action="${postReviewLink}" method="POST">
		<h4>Submit your Review</h4>
		<label for="username">Username</label>
		<input class="usernameText" type="text" name="username"/>
		<label for="rating">Rating</label>
		<select id="ratingDropdown" name="rating">
			<option value="1">1 Star</option>
			<option value="2">2 Star</option>
			<option value="3">3 Star</option>
			<option value="4">4 Star</option>
			<option value="5">5 Star</option>
		</select>
		<label for="reviewTitle">Title </label>
		<input id="titleInputBox" type="text" name="reviewTitle">
		<label for="reviewText">Review </label>
		<textarea rows="15" cols="80" name="reviewText"></textarea>
		<button id="reviewSubmitButton">Submit</button>
		</form>
		
	</body>
</html>
