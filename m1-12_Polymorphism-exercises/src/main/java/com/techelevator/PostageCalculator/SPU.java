package com.techelevator.PostageCalculator;

public class SPU implements Deliverable {
	private String name;
	private double ratePerPound;
	
	public SPU (double ratePerPound) {
		this.ratePerPound = ratePerPound;
	}
	
	@Override
	public double calculateRate(int distanceInMiles, double weightInOunces) {
		return (((weightInOunces / 16) * ratePerPound) * distanceInMiles);
	}
	public String getName() {
		return name;
	}
	
}
