package com.techelevator.PostageCalculator;

public class FirstClass extends PostalService{
	
	private String name = "Postal Service (1st Class)";

	public FirstClass() {
		super(.035, .040, .047, .195, .45, .50);
	}
//	private void setName() {
//		super.setName("Postal Service (1st Class)");
//	}
	@Override
	public String getName() {
		return name;
	}
}
