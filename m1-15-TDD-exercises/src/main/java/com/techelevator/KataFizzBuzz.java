package com.techelevator;

public class KataFizzBuzz {
	private String fizzBuzzIntAsString;
	private boolean hasThree = false;
	private boolean hasFive = false;
	
	public String fizzBuzz(int fizzBuzzInt) {
		setFizzBuzzIntAsString(fizzBuzzInt);
		
		hasThreeAndOrFive();
		
		if (isDivisibleBy3(fizzBuzzInt) && isDivisibleBy5(fizzBuzzInt) || hasThree && hasFive) {
			return "FizzBuzz";
		}
		if (isDivisibleBy3(fizzBuzzInt) || hasThree) {
			return "Fizz";
		}
		if (isDivisibleBy5(fizzBuzzInt) || hasFive) {
			return "Buzz";
		}
		if (isWithin1To100Inclusive(fizzBuzzInt)) {
			return fizzBuzzIntAsString;
		} else {
			return "";
		}
	}
	private void hasThreeAndOrFive() {
		for (int i = 0; i < fizzBuzzIntAsString.length(); i++) {
			if (fizzBuzzIntAsString.charAt(i) == '3') {
				hasThree = true;
			}
			if (fizzBuzzIntAsString.charAt(i) == '5') {
				hasFive = true;
			}
		}
	}
	private void setFizzBuzzIntAsString(int fizzBuzzInt) {
		fizzBuzzIntAsString = Integer.toString(fizzBuzzInt);
	}
	private boolean isDivisibleBy3(int fizzBuzzInt) {
		return (fizzBuzzInt % 3 == 0);
	}
	private boolean isDivisibleBy5(int fizzBuzzInt) {
		return (fizzBuzzInt % 5 == 0);
	}
	private boolean isWithin1To100Inclusive(int fizzBuzzInt) {
		return (fizzBuzzInt >= 1 && fizzBuzzInt <= 100);
	}
}
