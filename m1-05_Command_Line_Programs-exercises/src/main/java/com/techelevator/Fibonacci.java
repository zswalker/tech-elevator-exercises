package com.techelevator;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		System.out.println("Please enter the number you'd like to see the Fibonacci sequence leading up to.");
		int userNumber = in.nextInt();
		in.hasNextLine();
	
	
		int currentFibNumber = 0;
		int twoFibNumbersBefore = 0;
		int prevFibNumber = 0;
		int initialIncrement = 1; //to set up the intial 0 and 1 of the Fibonnaci sequence
		
		if (userNumber <= 0) {
			System.out.print("No numbers before 0");
		} 
			
		for (currentFibNumber=0; currentFibNumber < userNumber; currentFibNumber = twoFibNumbersBefore + prevFibNumber + initialIncrement) {
			if (currentFibNumber >= userNumber) {
				break;
			} else if (currentFibNumber == 1 && initialIncrement == 1) {
				System.out.print(currentFibNumber + ", ");
				initialIncrement = 0;
			} else if (currentFibNumber + prevFibNumber >= userNumber) {
				System.out.print(currentFibNumber);
			} else {
				System.out.print(currentFibNumber + ", ");
			}
			twoFibNumbersBefore = prevFibNumber;
			prevFibNumber = currentFibNumber;

			
		}

		
	}

}
