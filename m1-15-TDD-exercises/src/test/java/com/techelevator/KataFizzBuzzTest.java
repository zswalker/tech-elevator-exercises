package com.techelevator;

import org.junit.*;
import static org.junit.Assert.*;

public class KataFizzBuzzTest {
	private KataFizzBuzz test;
	
	@Before
	public void setup() {
		test = new KataFizzBuzz();
	}
	@Test
	public void number_divisible_by_3_returns_fizz() {
		String result = test.fizzBuzz(3);
		Assert.assertEquals("Fizz", result);
	}
	@Test
	public void number_divisible_by_5_returns_buzz() {
		String result = test.fizzBuzz(5);
		Assert.assertEquals("Buzz", result);
	}
	@Test
	public void number_divisible_by_3_and_5_returns_FizzBuzz() {
		String result = test.fizzBuzz(15);
		Assert.assertEquals("FizzBuzz", result);
	}
	@Test
	public void number_not_divisible_by_3_and_5_and_within_1_to_100_inclusive_returns_number_as_integer() {
		String result = test.fizzBuzz(4);
		Assert.assertEquals("4", result);
	}
	@Test
	public void number_outside_of_1_to_100_exclusive_return_empty_string() {
		String result = test.fizzBuzz(101);
		Assert.assertEquals("", result);
	}
	@Test
	public void number_contains_3_return_Fizz() {
		String result = test.fizzBuzz(13);
		Assert.assertEquals("Fizz", result);
	}
	@Test
	public void number_contains_5_return_Buzz() {
		String result = test.fizzBuzz(52);
		Assert.assertEquals("Buzz", result);
	}
	@Test
	public void number_contains_3_and_5_return_FizzBuzz() {
		String result = test.fizzBuzz(35);
		Assert.assertEquals("FizzBuzz", result);
	}
}
