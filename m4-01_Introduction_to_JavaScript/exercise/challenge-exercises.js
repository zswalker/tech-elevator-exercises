﻿function iqTest(numbers) {

    const numbersArr = numbers.split(" ");

    let isThereAnEven = false;
    let isThereAnOdd = false;

    for (let i = 0; i < numbersArr.length; i++) {
        if (numbersArr[i] % 2 == 0) {
            isThereAnEven = true;
        }

        if (numbersArr[i] % 2 == 1) {
            isThereAnOdd = true;
        }

        if (isThereAnEven && isThereAnOdd) {

            if (i == 1 && (numbersArr[1] % 2 == numbersArr[2] % 2)) {
                return i;
            }

            return i + 1;
        }
    }

    return 0;
}

function titleCase(title, minorWords = "") {
    let newTitle = "";
    let titleWordsArr = title.split(" ");

    titleWordsArr.forEach((word, index) => {
        titleWordsArr[index] = word.toLowerCase();
    });

    let minorWordsArr = minorWords.split(" ");

    minorWordsArr.forEach((word, index) => {
        minorWordsArr[index] = word.toLowerCase();
    });

    for (let i = 0; i < titleWordsArr.length; i++) {
        if (i == 0 || ! minorWordsArr.includes(titleWordsArr[i])) {
           let  newTitleWord = titleWordsArr[i].substring(0, 1).toUpperCase() + titleWordsArr[i].substring(1).toLowerCase();
            if (i != titleWordsArr.length - 1) {
                newTitle += newTitleWord + " ";
            } else {
                newTitle += newTitleWord;
            }
        } else if (minorWordsArr.includes(titleWordsArr[i])) {
                let newTitleWord =  titleWordsArr[i].toLowerCase();
                if (i != titleWordsArr.length - 1) {
                    newTitle += newTitleWord + " ";
                } else {
                    newTitle += newTitleWord;
                }
        }
    }
    return newTitle;
}