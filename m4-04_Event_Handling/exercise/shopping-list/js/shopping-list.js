let allItemsIncomplete = true;
const pageTitle = 'My Shopping List';
const groceries = [
  { id: 1, name: 'Oatmeal', completed: false },
  { id: 2, name: 'Milk', completed: false },
  { id: 3, name: 'Banana', completed: false },
  { id: 4, name: 'Strawberries', completed: false },
  { id: 5, name: 'Lunch Meat', completed: false },
  { id: 6, name: 'Bread', completed: false },
  { id: 7, name: 'Grapes', completed: false },
  { id: 8, name: 'Steak', completed: false },
  { id: 9, name: 'Salad', completed: false },
  { id: 10, name: 'Tea', completed: false }
];

/**
 * This function will get a reference to the title and set its text to the value
 * of the pageTitle variable that was set above.
 */
function setPageTitle() {
  const title = document.getElementById('title');
  title.innerText = pageTitle;
}

/**
 * This function will loop over the array of groceries that was set above and add them to the DOM.
 */
function displayGroceries() {
  const ul = document.querySelector('ul');
  groceries.forEach((item) => {
    const li = document.createElement('li');
    li.innerText = item.name;
    const checkCircle = document.createElement('i');
    checkCircle.setAttribute('class', 'far fa-check-circle');
    li.appendChild(checkCircle);
    ul.appendChild(li);
  });
}

document.addEventListener('DOMContentLoaded', () => {
  setPageTitle();
  displayGroceries();

  const ul = document.querySelector('ul');
  ul.addEventListener('click', (event) => {
    if (!event.target.classList.contains('completed') && event.target.matches('li')) {
      event.target.classList.add('completed');
      let icon = event.target.querySelector('i');
      icon.classList.add('completed')
    }

    if (!event.target.classList.contains('completed') && event.target.matches('i')) {
      event.target.classList.add('completed');
      let li = event.target.parentNode;
      li.classList.add('completed')
    }
  })

  ul.addEventListener('dblclick', (event) => {
    if (event.target.classList.contains('completed') && event.target.matches('li')) {
      event.target.classList.remove('completed');
      let icon = event.target.querySelector('i');
      icon.classList.remove('completed')
    }

    if (event.target.classList.contains('completed') && event.target.matches('i')) {
      event.target.classList.remove('completed');
      let li = event.target.parentNode;
      li.classList.remove('completed')
    }
  })

  const markAllButton = document.querySelector('#toggleAll');
  let liList = document.querySelectorAll('li');
  markAllButton.addEventListener('click', () => {
    if (allItemsIncomplete) {
      markAllButton.innerText = "MARK ALL INCOMPLETE";
      liList.forEach((li) => {
        li.classList.add('completed');
        li.querySelector('i').classList.add('completed');
      })
      
    } 
    if (! allItemsIncomplete) {
      markAllButton.innerText = "MARK ALL COMPLETE";
      liList.forEach((li) => {
        li.classList.remove('completed');
        li.querySelector('i').classList.remove('completed');
      })
      
    }

    allItemsIncomplete = !allItemsIncomplete;

  })
})


