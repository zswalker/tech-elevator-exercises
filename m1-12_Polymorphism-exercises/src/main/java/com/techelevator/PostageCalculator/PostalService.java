package com.techelevator.PostageCalculator;

public class PostalService implements Deliverable {
	//private Package packageToShip;
	private String name;
	private double rateFor0ToTwoOunces;
	private double rateFor3To8Ounces;
	private double rateFor9To15Ounces;
	private double rateFor1To3Pounds;
	private double rateFor4To8Pounds;
	private double rateFor9PlusPounds;
	
	public PostalService(double rateFor0ToTwoOunces, double rateFor3To8Ounces, double rateFor9To15Ounces, double rateFor1To3Pounds, double rateFor4To8Pounds, double rateFor9PlusPounds) {
		//this.packageToShip = packageToShip;
		this.rateFor0ToTwoOunces = rateFor0ToTwoOunces;
		this.rateFor3To8Ounces = rateFor3To8Ounces;
		this.rateFor9To15Ounces = rateFor9To15Ounces;
		this.rateFor1To3Pounds = rateFor1To3Pounds;
		this.rateFor4To8Pounds = rateFor4To8Pounds;
		this.rateFor9PlusPounds = rateFor9PlusPounds;
	}
	@Override
	public double calculateRate(int distanceToShipInMiles, double weightInOunces) {
		if (weightInOunces < 3 && weightInOunces >= 0) {
			return this.rateFor0ToTwoOunces * distanceToShipInMiles;
		} else if (weightInOunces < 9 && weightInOunces >= 3) {
			return this.rateFor3To8Ounces * distanceToShipInMiles;
		} else if (weightInOunces < 16 && weightInOunces >= 9) {
			return this.rateFor9To15Ounces * distanceToShipInMiles;
		} else if (weightInOunces < 64 && weightInOunces >= 16) {
			return rateFor1To3Pounds * distanceToShipInMiles;
		} else if (weightInOunces < 144 && weightInOunces >= 64) {
			return rateFor4To8Pounds * distanceToShipInMiles;
		} else {
			return rateFor9PlusPounds * distanceToShipInMiles;
		}
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
//	public double calculateRate() {
//			if (packageToShip.getWeightInOunces() < 3 && packageToShip.getWeightInOunces() >= 0) {
//				return this.rateFor0ToTwoOunces;
//			} else if (packageToShip.getWeightInOunces() < 9 && packageToShip.getWeightInOunces() >= 3) {
//				return this.rateFor3To8Ounces;
//			} else if (packageToShip.getWeightInOunces() < 16 && packageToShip.getWeightInOunces() >= 9) {
//				return this.rateFor9To15Ounces;
//			} else if (packageToShip.getWeightInOunces() < 64 && packageToShip.getWeightInOunces() >= 16) {
//				return rateFor1To3Pounds;
//			} else if (packageToShip.getWeightInOunces() < 144 && packageToShip.getWeightInOunces() >= 64) {
//				return rateFor4To8Pounds;
//			} else {
//				return rateFor9PlusPounds;
//			}
//		}
	
}
