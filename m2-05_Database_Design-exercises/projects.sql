CREATE TABLE departments
(
        department_id serial,
        name varchar(35) not null,
        
        constraint pk_department primary key (department_id)
);

CREATE TABLE projects
(
        project_id serial,
        name varchar(100) not null,
        start_date date not null,
        
        constraint pk_project primary key (project_id)
);

CREATE TABLE employees
(
        employee_id serial,
        job_title varchar(50) not null,
        last_name varchar(35) not null,
        first_name varchar(35) not null,
        gender varchar(6),
        other_gender varchar(15),
        birth_date date not null,
        hire_date date not null,
        department_id int not null,
        
        constraint pk_employee primary key (employee_id),
        constraint fk_employee_department foreign key (department_id) references departments (department_id),
        constraint chk_gender CHECK (gender IN ('Male', 'Female', 'Other')),
        constraint chk_other_gender CHECK ((gender = 'Other' AND other_gender IS NOT NULL) OR (gender <> 'Other' AND other_gender IS NULL))
);

CREATE TABLE projects_employees
(
        employee_id int not null,
        project_id int not null,
        
        constraint pk_projects_employees primary key (employee_id, project_id),
        constraint fk_projects_employees_employees foreign key (employee_id) references employees (employee_id),
        constraint fk_projects_employees_projects foreign key (project_id) references projects (project_id)
);


INSERT INTO departments (name)
VALUES ('Information Technology'), ('Sales'), ('Legal');

INSERT INTO projects (name, start_date)
VALUES ('Project X', '02/06/2019'), ('Top Secret Project', '01/01/2001'), ('Kind Of A Secret Project', '12/02/2018'), ('Not A Secret At All Project', '12/25/2018');

INSERT INTO employees (first_name, last_name, job_title, birth_date, hire_date, department_id)
VALUES
('Zach', 'Walker', 'President', '12/02/1990', '12/02/1990', 1),
('Bob', 'Barker', 'Developer', '12/12/1923', '01/03/2016', 1),
('David', 'Grohl', 'Sales Rep',  '01/14/1969', '05/16/2007', 2),
('John', 'Doe', 'Sales Manager', '08/24/1985', '09/18/2011', 2),
('Billy', 'Vegas', 'Lawyer', '12/06/1992', '03/11/2016', 3),
('Baker', 'Mayfield', 'Chief Legal Officer', '04/14/1995', '07/24/2018', 3),
('Jane', 'Doe', 'UX Designer', '11/12/1995', '06/27/2015', 1),
('Dude', 'Covington', 'QA Tester', '10/31/1979', '02/29/2016', 1);

INSERT INTO projects_employees (employee_id, project_id)
VALUES (1,1), (1,2), (2, 3), (3, 4), (4, 4), (6, 1), (7, 3), (8, 3);

SELECT * FROM projects_employees;

        