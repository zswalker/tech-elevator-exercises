package com.techelevator;

import org.junit.*;

public class SameFirstLastTest {
	private SameFirstLast test;
	
	@Before
	public void setup() {
		test = new SameFirstLast();
	}
	@Test
	public void array_length_1_or_more_and_first_and_last_equal_return_true() {
		int[] array = {1, 3, 4, 1};
		
		boolean result = test.isItTheSame(array);
		
		Assert.assertTrue(result);
	}
	@Test
	public void array_length_1_returns_true() {
		int[] array = {1};
		
		boolean result = test.isItTheSame(array);
		
		Assert.assertTrue(result);
	}
	@Test
	public void empty_array_returns_false() {
		int[] array = new int[0];
		
		boolean result = test.isItTheSame(array);
		
		Assert.assertFalse(result);
	}
	@Test
	public void array_length_1_or_more_and_first_and_last_different_return_false() {
		int[] array = {1, 3, 4, 2};
		
		boolean result = test.isItTheSame(array);
		
		Assert.assertFalse(result);
	}
}
