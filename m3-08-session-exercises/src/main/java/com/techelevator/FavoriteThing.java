package com.techelevator;

public class FavoriteThing {
	
	String favoriteColor;
	String favoriteFood;
	String favoriteSeason;
	
	
	public String getFavoriteColor() {
		return favoriteColor;
	}
	
	public void setFavoriteColor(String favoriteColor) {
		this.favoriteColor = favoriteColor;
	}
	
	public String getFavoriteFood() {
		return favoriteFood;
	}
	
	public void setFavoriteFood(String favoriteFood) {
		this.favoriteFood = favoriteFood;
	}
	
	public String getFavoriteSeason() {
		return favoriteSeason;
	}
	
	public void setFavoriteSeason(String favoriteSeason) {
		this.favoriteSeason = favoriteSeason;
	}



}
