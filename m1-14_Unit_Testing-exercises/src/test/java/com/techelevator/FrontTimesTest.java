package com.techelevator;

import org.junit.*;

public class FrontTimesTest {
	private FrontTimes test;
	
	@Before
	public void setup() {
		test = new FrontTimes();
	}
	@Test
	public void string_less_than_4_return_full_string_n_times() {
		String result = test.generateString("ab", 2);
		
		Assert.assertEquals("abab", result);
	}
	@Test
	public void string_greater_than_3_return_first_3_string_n_times() {
		String result = test.generateString("abcd", 2);
		
		Assert.assertEquals("abcabc", result);
	}
	@Test
	public void empty_string_return_empty_string() {
		String result = test.generateString("", 2);
		
		Assert.assertEquals("", result);
	}
	@Test
	public void null_string_returns_null() {
		String result = test.generateString(null, 2);
		
		Assert.assertEquals(null, result);
	}
	@Test
	public void negative_n_returns_empty_string() {
		String result = test.generateString("ab", -2);
		
		Assert.assertEquals("", result);
	}
	@Test
	public void mixed_case_string_returns_mixed_case_result() {
		String result = test.generateString("aB", 2);
		
		Assert.assertEquals("aBaB", result);
	}
	@Test
	public void zero_n_returns_empty_string() {
		String result = test.generateString("ab", 0);
		
		Assert.assertEquals("", result);
	}
}
